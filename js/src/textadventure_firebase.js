var App = App || {};
App.TextAdventureFirebase = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher();
    
    function testfunction() {
        console.log("Hello Database Firebase!");
    }
    
    that.testfunction = testfunction;
    return that;
};