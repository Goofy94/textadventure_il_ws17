var App = App || {};
App.Textadventure = (function () {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher */
    
    var that = new EventPublisher(),
        controller, firebase, model, view,
        pencil, schedule, brezel, key, passport, crib,
        exam, paper,
        professor, lecturer,
        campus, mensa, cip2, h6, pt, zhg, office,
        officeTimeoutSteps, officeTimeoutProfessor,
        gameState = "SETTINGS"; //Auf "SETTINGS" stellen um den Anfang zu erhalten
    
    function init() {
        initJavaScripts();
        initHTMLElements();
        addEventlistenersToJavaScripts();
        initGame();
    }
    
    
    // INIT ALL JAVA SCRIPT ELEMENTS
    function initJavaScripts() {
        controller = new App.TextAdventureController();
        
        initObjects();
        initPersons();
        initRooms();
        
        controller.initDifferentRooms(campus, mensa, cip2, h6, pt, zhg, office);
        controller.initCharacters(professor, lecturer);
        
        firebase = new App.TextAdventureFirebase();
        model = new App.TextAdventureModel();
        
        controller.initModel(model);
        view = new App.TextAdventureView();
    }
    
    function initObjects() {
        pencil = new App.TextAdventureObject();
        pencil.init({name: "Stift", edible: false, description: "Ein gelber, klassischer Bleistift. Du könntest deine Aufgaben damit bearbeiten.", editable: false});
        pencil = pencil.getProperties();
        
        schedule = new App.TextAdventureObject();
        schedule.init({name: "Stundenplan", edible: false, description: "Auf dem Stundenplan steht für heute: <br>Informationslinguistik, 08:15 Uhr, Hörsaal 6<br>Mittagspause, Mensa<br>Mathematische Grundlagen, 12:00 Uhr, CIP Pool 2", editable: false});
        schedule = schedule.getProperties();
        
        brezel = new App.TextAdventureObject();
        brezel.init({name: "Breze", edible: true, description: "Eine leckere, saftige Breze.", editable: false});
        brezel = brezel.getProperties();
        
        key = new App.TextAdventureObject();
        key.init({name: "Schlüssel", edible: false, description: "Ein Schlüssel. Ob er für ein bestimmtes Schloss passen mag?", editable: false});
        key = key.getProperties();
        
        passport = new App.TextAdventureObject();
        passport.init({name: "Ausweis", edible: false, description: "Ein Universitätsausweis. Er liegt bestimmt schon einige Zeit auf dem Boden. Er ist etwas dreckig und du kannst ihn nicht komplett entziffern. Einige Buchstaben kannst du jedoch lesen.<br>Vorname: P#te# <br> Name: Pr#to#<br>Wem mag dieser Ausweis wohl gehören?", editable: false});
        passport = passport.getProperties();
        
        exam = new App.TextAdventureObject();
        exam.init({name: "Studienleistung", edible: false, description: "Eine Studienleistung für das Fach Informationslinguistik. Sie behandelt das Thema 'Reguläre Ausdrücke'. Es gibt mehrere Hinweise und Hilfen, die im TextadventURe versteckt sind. Eine gute Basis bieten jedoch die Erläuterungen von Professor Proton. Du hast zwei Versuche für diese Studienleistung. Wenn du dich bereit fühlst, kannst die Aufgabe <b>bearbeiten</b> und sie anschließend Professor Proton geben. Wenn du die Aufgabe schließen möchtest, dann drücke einfach auf das rote 'X' in der oberen rechten Ecke. Deine Ergebnisse bleiben gespeichert, sofern du sie eingetragen hast.", editable: true});
        exam = exam.getProperties();
        
        paper = new App.TextAdventureObject();
        paper.init({name: "Übungsblatt", edible: false, description: "Ein Übungblatt für das Fach Mathematische Grundlagen. Sie behandelt das Thema 'Binärzahlen'. Es gibt mehrere Hinweise und Hilfen, die im TextadventURe versteckt sind. Eine gute Basis bieten jedoch die Erläuterungen von Dozent Dietrich. Du hast nur einen Versuch für diese Übung. Wenn du dich bereit fühlst, kannst die Aufgabe <b>bearbeiten</b> und sie anschließend Dozent Dietrich geben. Wenn du die Aufgabe schließen möchtest, dann drücke einfach auf das rote 'X' in der oberen rechten Ecke. Deine Ergebnisse bleiben gespeichert, sofern du sie eingetragen hast.", editable: true});
        paper = paper.getProperties();
        
        crib = new App.TextAdventureObject();
        crib.init({name: "Spickzettel", edible: false, description: "Ein abgerissenes Stück Papier. Es stammt offenbar von einer Studienleistung. Es wäre natürlich unmoralisch den Spickzettel zu benutzen, aber ... du kannst noch einige Sätze lesen.<br>Antwort Aufgabe 3) [125]+<br>Antwort Aufgabe 4) [1-...<br>Mehr kannst du leider nicht erkennen.", editable: false});
        crib = crib.getProperties();
    }
    
    function initPersons() {
        // Der Professor steht in H6 und lehrt INFORMATIONSLINGUISTIK
        professor = new App.TextAdventurePerson();
        professor.init({name: "Professor Proton", talk: "Grüß Gott. Mein Name ist Professor Proton. Ich bin der Professor für <b>Informationslinguistik</b>. Wenn du mehr über dieses Thema erfahren willst, dann frag mich einfach nach dem Stichwort Informationslinguistik. In diesem Fach musst du eine Studienleistung abgeben. Du hast dafür zwei Versuche und kannst sie jeweils bei mir abgeben. Ansonsten wünsche ich einen angenehmen Tag.", objects: [exam], teachingBasic: "Reguläre Ausdrücke sind eine Art Sprache, die beim Programmieren für diverse Problemlösungen verwendet werden kann, insbesondere dann, wenn es darum geht, Zeichenketten (auch Strings genannt) oder Texte nach bestimmten Mustern zu durchsuchen. Da der Name 'Reguläre Ausdrücke' etwas unhandlich ist, werden sie auch oft einfach nur als 'RegEx' bezeichnet (vom engl. Regular Expressions).<br>Sie können als Filterkriterien in der Textsuche verwendet werden, indem der Text mit dem Muster des regulären Ausdrucks abgeglichen wird. Dieser Vorgang wird auch Pattern Matching genannt. So ist es beispielsweise möglich, alle Wörter aus einer Wortliste herauszusuchen, die mit 'S' beginnen und auf 'D' enden, ohne die dazwischen liegenden Buchstaben oder deren Anzahl explizit vorgeben zu müssen.<br>Die einfachste Form eines RegEx ist eine in eckigen Klammern ([ und ]) angegebene Zeichenauswahl. So findet z.B. der Ausdruck [a] ein kleines 'a' in einem Text, der Ausdruck [4] die Ziffer '4'. Der Ausdruck in eckigen Klammern steht für genau ein Zeichen aus dieser Auswahl. Das heißt, dass der Ausdruck [abc] entweder ein 'a' oder 'b' oder 'c' findet, nicht alle drei zusammen 'abc'. Innerhalb dieser Zeichenklassendefinitionen haben einige Symbole z.B. der Bindestrich '-' andere Bedeutungen als im normalen Kontext. Dieser wird verwendet um einen Bereich/Intervall anzugeben. Deshalb kann man die Ziffern 1, 2, 3 und 4 mit dem RegEx [1234] oder auch mit dem kürzeren Ausdruck [1-4] finden.<br>Wenn Sie mehr Beispiele über <b>Zeichendefinitionen</b> sehen wollen, dann sag es einfach. Ansonsten fahre ich mit dem Vortrag fort, indem Sie mich nach dem Stichwort <b>Mehrstellig</b>e Ausdrücke fragen.", teachingExamples: "Ich gebe Ihnen gerne einige weitere Beispiele:<br><b>[egh]:</b> Eines der Zeichen 'e', 'g' oder 'h'<br><b>[0-6]:</b> Eine Ziffer von '0' bis '6' (Bindestriche sind Indikator für einen Bereich)<br><b>[A-Za-z0-9]:</b> Ein beliebiger lateinischer Buchstabe oder eine beliebige Ziffer. Innerhalb der eckigen Klammern wird zwischen Groß- und Kleinschreibung unterschieden.<br><b>[1-35-9]:</b> Eine beliebige Ziffer zwischen '1' und '9', ausgenommen die Ziffer '4'. Hierbei ist zu beachten, dass nicht der Bereich zwischen '1' und '35' gesucht wird, da die Zeichen innerhalb der eckigen Klammern immer einzeln betrachtet werden. Ergo gibt es die Bereiche '1' bis '3' und '5' bis '9'. Die Bereiche werden auch nicht durch ein Leerzeichen getrennt, wie man es vielleicht intuitiv machen möchte, da der Ausdruck ansonsten auch explizit das Leerzeichen suchen würde.<br>Ich mache mit dem Vortrag weiter, wenn Sie mich nach dem Stichwort <b>Mehrstellig</b>e Ausdrücke fragen.", teachingTwoDigit: "Reguläre Ausdrücke können sehr schnell kompliziert werden. Oft möchte man ja nicht nur einzelne Ziffern oder Zeichen finden, sondern komplexere Strukturen. In folgendem Szenario geht es darum, Gleise eines Bahnhofs in einem Text zu suchen. Dabei können die Gleise von 1 bis 9 nummeriert sein und jeweils noch einen Abschnitt a, b, c oder d enthalten. Es ergeben sich also z.B. die Kombinationen 1a, 3b, 9c usw. Für unseren RegEx bedeutet das, dass wir zweistellige Strings (Zeichenketten) finden müssen. Stellen Sie sich dazu vor, dass jedes Wort/Zahl so viele Stellen besitzt, wie es Buchstaben/Ziffern hat, also 'Hallo' hat fünf Stellen; die Zahl 1011 hat vier Stellen.<br>Zunächst suchen wir also für die Gleise wie gewohnt die Gleisnummer mit dem Ausdruck [1-9] für die erste Stelle. An diesen hängen wir die Gleisabschnitte [a-d] für die zweite Stelle an, sodass sich der Ausdruck <br><b>[1-9][a-d]</b> ergibt.<br>Dieses Konzept kann man auf beliebig viele Stellen erweitern, was man an folgendem RegEx erkennen kann <br><b>[1-4][ab][cd]</b>. Dieser findet z.B. die dreistelligen Kombinationen 1ac, 4bd, usw., jedoch nicht die Zeichenkette 1ab, da es an zweiter Stelle entweder das 'a' oder das 'b' geben kann und an dritter Stelle ein 'c' oder 'd' stehen muss.<br>Abschließend möchte ich die Vorlesung mit dem Thema <b>Quantoren</b> beenden. Fragen Sie nur danach.", teachingQuantifiers: "Quantoren (englisch quantifier, auch Quantifizierer oder Wiederholungsfaktoren) erlauben es, den vorherigen Ausdruck in verschiedener Vielfachheit in der Zeichenkette zuzulassen. Mithilfe von Quantoren kann man einen dynamischen Ausdruck erzeugen, wie Sie anhand des folgenden Beispiels erkennen werden. Wenn man z.B. eine Telefonnummer in einem Text suchen will, kann man sich vorstellen, dass es mit den bisher erlernten Möglichkeiten ziemlich schwierig und mühselig ist, einen zehnstelligen regulären Audruck zu erstellen, der eine Nummer findet. Doch was passiert, wenn es im Text eine Telefonnummer mit nur neun Stellen gibt? Oder eine mit 15? Dafür gibt es Quantoren, die uns bei diesem Problem helfen. Die drei wichtigsten Quantoren sind das Fragezeichen '<b>?</b>', das Plus '<b>+</b>' und der Kleene-Stern '<b>*</b>'<br><b>?</b>: Der voranstehende Ausdruck ist optional, er kann einmal vorkommen, braucht es aber nicht, das heißt, der Ausdruck kommt null- oder einmal vor. Bsp.: [1-4][a]<b>?</b> findet z.B. die Kombination 1a, 2a oder 4a, aber auch z.B. nur die Ziffern '1', '2' oder '3', da das 'a' durch den ?-Quantor optional ist.<br><b>+</b>: Der voranstehende Ausdruck muss mindestens einmal vorkommen, darf aber auch mehrfach vorkommen. Bsp.: [a]<b>+</b> findet a, aa, aaaaaa, usw., da das 'a' beliebig oft vorkommen darf, jedoch mindestens 1-mal<br><b>*</b>: Der voranstehende Ausdruck darf beliebig oft (auch keinmal) vorkommen. Bsp.: [a]<b>*</b> findet ebenfalls a, aa, aaaaaa jedoch auch die leere Menge {}, da das 'a' beliebig oft vorkommen darf, auch 0-mal.<br>Um auf unser anfängliches Beispiel mit den Telefonnummern zurückzukommen: Man könnte zum Beispiel den Ausdruck <b>[0-9]+[/]?[0-9]+</b> benutzen. Dieser findet alle Ziffernkombinationen, die aus mindestens zwei Ziffern bestehen, also z.B. 018001234 oder 123456789. Auch findet er z.B. die Kombination 0162/123456789, da das '/' innerhalb der Ziffernfolge einmal oder keinmal vorkommen darf.<br>Möchten Sie noch einmal eine <b>Zusammenfassung</b> aller erlernten Punkte haben? Ansonsten ist die Vorlesung beendet. Vielen Dank für Ihre Aufmerksamkeit.", teachingSummary: "Ein RegEx kann innerhalb von eckigen Klammern geschrieben werden. Darin befinden sich Zeichendefinitionen, die angeben nach welchen Zahlen/Ziffern/Zeichenketten der Ausdruck suchen soll.<br>Jedes Paar aus eckigen Klammern steht für eine Stelle in einem Wort oder einer Zahl. [][][] = 3-stellig, [] = 1-stellig<br><b>[abc]</b>: 'a' oder 'b' oder 'c'<br><b>[1-9]</b>: Ziffern zwischen '1' und '9'<br><b>[1-4][a-d]</b>: Zweistelliger Ausdruck. Kombinationen 1a, 2b, 3a, 4d, usw.<br><b>[1-4][a]?</b>: Kombinationen aus den Ziffern '1' bis '4' und ein optionales 'a'. Also 1a, 2a, 3, 3a, usw.<br><b>[ab]+</b>: Mindestens ein 'a' oder 'b', beliebig viele Weitere. Also a, abab, aaaaa, bbbbb, abaaabababa, usw.<br><b>[ab]*</b>: Ein oder Kein 'a' oder 'b', beliebig viele Kombinationen aus Beiden. Also leere Menge {}, abab, aaa, bbbb, ababababb, usw.<br>Sie können mich jederzeit unabhängig von den vorherigen Erläuterungen nach der Zusammenfassung fragen."});
        
        // Der Dozent steht in CIP2 und lehrt MATHEMATISCHE GRUNDLAGEN
        lecturer = new App.TextAdventurePerson();
        lecturer.init({name: "Dozent Dietrich", talk: "Hallo. Mein Name ist Dozent Dietrich. Ich lehre die Kunst der Mathematischen Grundlagen. Zur Zeit geht es hier um das Binärsystem oder auch <b>Dualsystem</b> genannt. Dieses Wissen ist entscheident, da wir verstehen müssen wie Computer funktionieren und diese ihre Berechnungen auf dieses System stützen. Wenn du mehr darüber erfahren willst, dann frag mich einfach nach dem Stichwort Dualsystem. In diesem Kurs musst du regelmäßig Übungsblätter abgeben. Du hast dafür nur einen Versuch, also streng dich an.<br>Machen Sie es gut.", objects: [paper], teachingBinaryBasic: "Das Dualsystem (lat. dualis „zwei enthaltend“), auch Zweiersystem oder Binärsystem genannt, ist ein Zahlensystem, das zur Darstellung von Zahlen nur zwei verschiedene Ziffern benutzt. Im üblichen Dezimalsystem werden die Ziffern 0 bis 9 verwendet. Im Dualsystem hingegen werden Zahlen nur mit den Ziffern des Wertes null und eins dargestellt. Oft werden für diese Ziffern die Symbole 0 und 1 verwendet. Aufgrund seiner Bedeutung in der Digitaltechnik ist es neben dem Dezimalsystem das wichtigste Zahlensystem. Die Zahldarstellungen im Dualsystem werden auch Dualzahlen oder Binärzahlen genannt.<br>Bei der Zahldarstellung im Dualsystem werden die Ziffern wie im gewöhnlich verwendeten Dezimalsystem ohne Trennzeichen hintereinander geschrieben, ihr Stellenwert entspricht allerdings der zur Stelle passenden Zweierpotenz und nicht der Zehnerpotenz.<br>Bevor ich Ihnen gleich das Dualsystem anhand eines Beispiels erkläre, werfen wir zunächst einmal einen Blick auf das uns wohlbekannte Dezimalsystem, da alle Zahlensysteme analog zueinander funktionieren. Im Dezimalsystem verwendet man die zehn Ziffern 0-9, welche als Dezimalziffern bezeichnet werden. Jede Ziffer hat einen Ziffernwert und je nach Position einen Stellenwert. Der Ziffernwert liegt in der konventionellen Zählreihenfolge. Der Index <b>i</b> legt den Stellenwert fest, dieser ist die Zehnerpotenz 10<sup>i</sup>. Lange Rede wenig Sinn: Die Dezimalzahl 384 ergibt sich also aus der Addition der Produkte der Ziffern mit dem Index <b>i</b> zur Basis Zehn. Dabei ist <b>i</b> für die Ziffer an der rechten Stelle immer gleich 0 und wird nach links hin immer um 1 aufaddiert. Das bedeutet:<br>384 =<br>3 <b>&middot;</b> 10<sup>2</sup> + 8 <b>&middot;</b> 10<sup>1</sup> + 4 <b>&middot;</b> 10<sup>0</sup> =<br>3 <b>&middot;</b> 100 + 8 <b>&middot;</b> 10 + 4 <b>&middot;</b> 1 =<br>300 + 80 + 4 = 384.<br>Wenn Sie dieses Konzept einmal verinnerlicht haben, dann machen wir mit einem <b>Beispiel</b> weiter wenn Sie es wollen. Dann sollten sie das Dualsystem verstanden haben.", teachingBinarySystem: "Lassen Sie mich Ihnen das Dualsystem anhand von folgendem Beispiel erklären: [13]<sub>10</sub> ist die Zahl 13 im Dezimalsystem, [1101]<sub>2</sub> hingegen stellt nicht (wie im Dezimalsystem) die Tausendeinhunderteins dar, sondern in ihrem Zahlensystem ebenfalls die Dreizehn, denn im Dualsystem berechnet sich der Wert durch:<br>&bull; [1101]<sub>2</sub> = 1 <b>&middot;</b> 2<sup>3</sup> + 1 <b>&middot;</b> 2<sup>2</sup> + 0 <b>&middot;</b> 2<sup>1</sup> + 1 <b>&middot;</b> 2<sup>0</sup> = 1 &middot; 8 + 1 &middot; 4 + 0 &middot; 2 + 1 &middot; 1 = 8 + 4 + 0 + 1 = [13]<sub>10</sub><br>Ich kann Ihnen auch noch direkt ein weiteres Beispiel geben:<br>&bull; [11111]<sub>2</sub> = 1 <b>&middot;</b> 2<sup>4</sup> + 1 <b>&middot;</b> 2<sup>3</sup> + 1 <b>&middot;</b> 2<sup>2</sup> + 1 <b>&middot;</b> 2<sup>1</sup> + 1 <b>&middot;</b> 2<sup>0</sup> = 1 &middot; 16 + 1 &middot; 8 + 1 &middot; 4 + 1 &middot; 2 + 1 &middot; 1 = 16 + 8 + 4 + 2 + 1 = [31]<sub>10</sub><br>Die Klammerung der Resultate mit der tiefgestellten 2 beziehungsweise der 10 gibt die Basis des verwendeten Stellenwertsystems an. So kann leicht erkannt werden, ob die Zahl im Dual- oder im Dezimalsystem dargestellt ist. In der Literatur werden die eckigen Klammern oft weggelassen und die tiefergestellte Zahl dann manchmal in runde Klammern gesetzt. Ebenfalls möglich ist die Kennzeichnung durch ein vorangestelltes '0b'. Demnach ist im Dualsystem die Zahl 1011 = [1011]<sub>2</sub> = 0b1011.<br>Ich hoffe, dass ich Ihnen das Dualsystem näher bringen konnte. Benutzen Sie gerne auch die Lernsoftware auf dem <b>Computer</b> neben mir. Ich muss weiter zur nächsten Vorlesung. Bis Bald."});
    }
    
    function initRooms() {
        campus = new App.TextAdventureRoom();
        mensa = new App.TextAdventureRoom();
        cip2 = new App.TextAdventureRoom();
        h6 = new App.TextAdventureRoom();
        pt = new App.TextAdventureRoom();
        zhg = new App.TextAdventureRoom();
        office = new App.TextAdventureRoom();
        
        campus.init({name: "campus", objects: [pencil, schedule], places: ["mensa", "pt", "zhg", "philosophie", "theologie", "zentrales", "gebäude"], persons: [], description: "Auf dem Campus herrscht das hektische Treiben des Uni-Alltags. Das Wahrzeichen der Universität, die Kugel, befindet sich hier. Hinter dir befindet sich die <b>Mensa</b>. In weiterer Entfernung kannst du das Philosophie und Theologie <b>(PT)</B> sowie das Zentrale Hörsaal Gebäude <b>(ZHG)</b> sehen."});
        
        mensa.init({name: "mensa", objects: [brezel], places: ["campus"], persons: [], description: "In der Mensa riecht es vorzüglich. Leckeres, frisch zubereitetes Essen steht in der Auslage. Darüber kannst du den " + "<a href='https://stwno.de/de/gastronomie/speiseplan/uni-regensburg/mensa-mittags' target='_blank'>Speiseplan</a>" + " sehen. Abgesehen vom Essen kannst du hier auch sehr gut <b>entspannen</b> und zu neuen Kräften kommen. Du hast Zugang auf den <b>Campus</b>."});
        
        cip2.init({name: "cip2", objects: [], places: ["pt", "philosophie", "theologie"], persons: [lecturer], description: "Ein hochmoderner Computerraum. Hier kannst du recherchieren oder auch arbeiten. Drucker befinden sich ebenfalls hier, falls du welche suchst. Manche Kurse finden hier statt, wenn sie einen PC benötigen. Der Dozent packt gerade seine Sachen zusammen. Offenbar findet die Vorlesung 'Mathematische Grundlagen' heute nicht statt. Die Tür führt zurück in den Flur vom Philosophie und Theologie <b>(PT)</b> Trakt."});
        
        h6.init({name: "h6", objects: [], places: ["zhg", "zentrales", "gebäude"], persons: [professor], description: "Ein sehr großer Hörsaal. Unzählige Stuhlreihen für hunderte Studenten befinden sich hier. In dem großen Hörsaal findet einmal die Woche die Vorlesung 'Informationslinguistik' zwischen 08:15 - 09:45 Uhr statt. Die Vorlesung beginnt wenn du mit dem Professor sprichst. Eine Tür führt zurück in das Zentrale Hörsaal Gebäude <b>(ZHG)</b>."});
        
        pt.init({name: "pt", objects: [passport], places: ["campus", "büro", "cip", "cip2"], persons: [], description: "Hier befindet sich das " + "<a href='http://www.uni-regensburg.de/sprache-literatur-kultur/informationswissenschaft/kontakt/index.html' target='_blank'>Sekretariat</a>" + " und die Büros deiner zukünftigen Dozenten. Auf dem <b>Büro</b> direkt vor dir steht 'Büro PT 1.02, Peter Proton'. Auch die Tür zu einem Computerraum <b>(CIP Pool 2)</b> steht weit offen. Die quitschenden Türen führen zurück auf den <b>Campus</b>."});
        
        zhg.init({name: "zhg", objects: [key], places: ["h6", "campus", "hörsaal6", "hörsaal", "h"], persons: [], description: "Das Herz der Universität. Du erreichst jeden wichtigen Hörsaal von hier aus. Große Stufen laden fast dazu ein sich hinzusetzen und vielleicht ein bisschen in deiner Tasche zu stöbern. Viele Studenten entspannen oder machen auch ihre Arbeiten darauf. Vor dir befindet sich der Eingang in den <b>Hörsaal 6</b>. Neben dir steht die Tür auf den <b>Campus</b> weit offen."});
        
        office.init({name: "büro", objects: [crib], places: ["pt", "philosophie", "theologie"], persons: [], description: "Du befindest dich im Büro von Professor Peter Proton. Ob du überhaupt hier sein darfst? Beeil dich lieber und verschwinde, bevor er zurück kommt und du mächtigen Ärger bekommst!"});
    }
    
    
    // INIT THE HTML ELEMENTS
    function initHTMLElements() {
        initStudyExerciseElements();
        initPaperExerciseElements();
        initComputerBinarySW();
    }
    
    function initStudyExerciseElements() {
        var studyBackButton, study1Button, study2Button, study3Button, study4Button;
        
        studyBackButton = document.getElementById("study_back_button");
        study1Button = document.getElementById("button_study_1");
        study1Button.addEventListener("click", onStudyAnswerButtonClicked);
        
        study2Button = document.getElementById("button_study_2");
        study2Button.addEventListener("click", onStudyAnswerButtonClicked);
        
        study3Button = document.getElementById("button_study_3");
        study3Button.addEventListener("click", onStudyAnswerButtonClicked);
        
        study4Button = document.getElementById("button_study_4");
        study4Button.addEventListener("click", onStudyAnswerButtonClicked);
        
        studyBackButton = document.getElementById("study_back_button");
        studyBackButton.addEventListener("click", onBackButtonClicked);
    }
    
    function initPaperExerciseElements() {
        var paperBackButton, paper1Button, paper2Button, paper3Button, paper4Button;
        
        paper1Button = document.getElementById("button_paper_1");
        paper1Button.addEventListener("click", onPaperAnswerButtonClicked);
        
        paper2Button = document.getElementById("button_paper_2");
        paper2Button.addEventListener("click", onPaperAnswerButtonClicked);
        
        paper3Button = document.getElementById("button_paper_3");
        paper3Button.addEventListener("click", onPaperAnswerButtonClicked);
        
        paper4Button = document.getElementById("button_paper_4");
        paper4Button.addEventListener("click", onPaperAnswerButtonClicked);
        
        paperBackButton = document.getElementById("paper_back_button");
        paperBackButton.addEventListener("click", onBackButtonClicked);
    }
    
    function initComputerBinarySW() {
        var com64, com32, com16, com8, com4, com2, com1, comBack, comHack;
        com64 = document.getElementById("computer_64");
        com32 = document.getElementById("computer_32");
        com16 = document.getElementById("computer_16");
        com8 = document.getElementById("computer_8");
        com4 = document.getElementById("computer_4");
        com2 = document.getElementById("computer_2");
        com1 = document.getElementById("computer_1");
        comBack = document.getElementById("computer_back_button");
        comHack = document.getElementById("computer_hack");
        
        com1.addEventListener("click", onComputerDigitClicked);
        com2.addEventListener("click", onComputerDigitClicked);
        com4.addEventListener("click", onComputerDigitClicked);
        com8.addEventListener("click", onComputerDigitClicked);
        com16.addEventListener("click", onComputerDigitClicked);
        com32.addEventListener("click", onComputerDigitClicked);
        com64.addEventListener("click", onComputerDigitClicked);
        comBack.addEventListener("click", onBackButtonClicked);
        comHack.addEventListener("click", onComputerButtonHacked);
        
        // Wird benutzt um die erstel Zahl random zu erzeugen/initialisieren
        controller.handleMatchingNumbers();
    }
    
    
    // ADDING EVENT LISTENERS TO THE JAVA SCRIPTS
    function addEventlistenersToJavaScripts() {
        controller.addEventListener("updateModelWithUsername", onUsernameEntered);
        controller.addEventListener("acceptModelWithUsername", onUsernameAccepted);
        controller.addEventListener("declineModelWithUsername", onUsernameDeclined);
        controller.addEventListener("startTheGame", onGameStarted);
        controller.addEventListener("computerBinaryStarted", onComputerStarted);
        controller.addEventListener("officeTimeStart", onOfficeTimeStart);
        controller.addEventListener("restartGame", onGameRestarted);
        controller.addEventListener("goPTstopTimer", stopOfficeTimeouts);
        
        campus.addEventListener("objectTaken", onObjectTaken);
        mensa.addEventListener("objectTaken", onObjectTaken);
        cip2.addEventListener("objectTaken", onObjectTaken);
        h6.addEventListener("objectTaken", onObjectTaken);
        pt.addEventListener("objectTaken", onObjectTaken);
        zhg.addEventListener("objectTaken", onObjectTaken);
        office.addEventListener("objectTaken", onObjectTaken);
        
        professor.addEventListener("objectTaken", onObjectTaken);
        professor.addEventListener("exercisePointsUpdated", onExercisePointsUpdated);
        professor.addEventListener("takeBackStudyPoints", onStudyPointsTakeBack);
        lecturer.addEventListener("objectTaken", onObjectTaken);
        lecturer.addEventListener("exercisePointsUpdated", onExercisePointsUpdated);
        
        model.addEventListener("currentGameStateUpdated", onGameStateUpdated);
        model.addEventListener("objectEdit", onObjectEdit);
        
        view.addEventListener("hiddenComputer", onHiddenComputerDiscovered);
    }
    
    
    // ++++++++++ INIT GAME ++++++++++
    /* Je nach gameState (START; SETTINGS) wird das Spiel initialisiert */
    function initGame() {
        if (gameState === "SETTINGS") {
            view.displaySystemMessage("Wie möchtest du im TextadventURe genannt werden?");
        }
        initGameStart();
    }
    
    /* Füge Eventlistener auf den Button 'Eingabe' und auf die Enter-Taste */
    function initGameStart() {
        var clickButton, inputContainer;
        
        /* This adds the clickEvent to the Button to put the Message into the Messageboard */
        clickButton = document.getElementById("clickMeButton");
        clickButton.addEventListener("click", onButtonOrEnterClicked);
        
        /* This adds the keyEvent (Enter pressed) to the inputfield to put the Message into the Messageboard */
        inputContainer = document.querySelector(".messageInput");
        inputContainer.addEventListener("keydown", function (event) {
            if (event.key === "Enter"){
                event.preventDefault();
                onButtonOrEnterClicked();
            }
        })
    }
    
    /* Eingaben werden geleert und das Spiel mit dem Zusatz 'START' erneut initialisiert */
    function onGameStarted() {
        view.clearConsoleAndStartGame();
        gameState = "START";
        initGame();
    }
    
    /* Um das Spiel am Ende erneut durchzuspielen, reicht es aus die Seite zu refreshen/neu zu laden */
    function onGameRestarted() {
        location.reload();
    }
    
    
    // ++++++++++ SETTINGS UND EINGABE ++++++++++
    /* If the Button is clicked, the String in the input field will be saved and checked that it is not empty. 
        Then the complete String will be given to the Controller, where the RegEx magic happens,
        Also the Input Container value will be set to an empty string = empty*/
    function onButtonOrEnterClicked() {
        var messageString, inputContainer, currentPlace;
        
        currentPlace = model.getCurrentPlace();
        
        inputContainer = document.getElementById("messageInput");
        messageString = inputContainer.value;
        
        if (gameState === "SETTINGS") {
            inputContainer.value = "";
            controller.handleSettings(messageString);
        } else if (gameState === "START") {
            if (/^[ a-zA-ZäöüÄÖÜ0-9]+$/.test(messageString)){
                inputContainer.value = "";
                controller.handleNewMessage(messageString, currentPlace);
            } else if (/[@#$%^&*()_+\-=\[\]{};': "\\|,.<>\/?]/g.test(messageString)) {
                // Sonst passiert es immer 2 mal?!
                inputContainer.value = "";
                view.displaySystemMessage("A#*~//rgh! >.< Bitte benutze keine Sonderzeichen.")
            }
        }
    }
    
    /* Wenn ein Name eingegeben wird, wird er im Model gespeichert und im View angezeigt */
    function onUsernameEntered(usernameObject) {
        model.saveUsername(usernameObject.data);
        view.saveUsername(usernameObject.data);
    }
    
    /* Letzer Schritt der Settings. Username wurde akzeptiert und das Spiel beginnt wenn der Nutzer 'START' eintippt */
    function onUsernameAccepted(acceptObject) {
        var name;
        name = model.getCurrentUsername();
        
        view.displaySystemMessage("Herzlich Willkommen " + name + ", <br> dies ist ein Textadventure zum Thema 'Dein erster Tag an der Universität Regensburg mit deinen Fächern Medieninformatik und/oder Informationswissenschaft'. Dir werden verschiedene Gebäude auf dem Campus und unterschiedliche Inhalte zu typischen Themengebieten deiner Fächerwahl näher vorgestellt. Interagiere mit verschiedenen Kommandos, löse Rätsel und mache deine Aufgaben, um den Tag zu absolvieren. Gib deine Kommandos am Besten folgendermaßen: Objekt Aktion (Stift aufheben) und achte auf <b>fett</b> geschriebene Stichworte. Im Hilfemenü findest du Tipps wenn du einmal nicht weiterkommst.");
        view.displaySystemMessage("Tippe 'START' ein um das Spiel zu beginnen");
    }
    
    /* Nachfrage, ob der Name auch korrekt ist */
    function onUsernameDeclined(declineObject) {
        view.displaySystemMessage("Wie möchtest du anstatt dessen heißen?")
    }
    
    /* Hier wird das aktualisierte gameStateObject vom Model an den View weitergegeben, um die Informationen anzuzeigen */
    function onGameStateUpdated(gameStateObject) {
        /* Ich bekomme das GameStateObject und leite es an das Model weiter, damit dort die Infos gezeigt werden können */
        view.showUpdatedGameState(gameStateObject.data);
    }
    
    
    // ++++++++++ INTERAKTION ++++++++++
    // +++++ COMPUTER +++++
    /* Der View zeigt den Computerbildschirm an, versteckt die Messages und deaktiviert die Eingabe und den Button */
    function onComputerStarted() {
        view.showComputerDisplay();
    }
    
    /* Wird aufgerufen, wenn auf eine Ziffer geklickt wurde. Der View ändert die Ziffer, der Controller rechnet die Dualzahl ins Dezimalsystem um, sie wird im Model gespeichert, und anschließend im View angezeigt */
    function onComputerDigitClicked(object) {
        var resultNumber, binaryContainer;
        // Container für den View, um die richtige Stelle zu ändern (1->0 und 0->1)
        binaryContainer = object.target.id;
        view.setBinaryDigit(binaryContainer);
        
        resultNumber = controller.handleDigitClick();
        
        model.storeCurrentResult(resultNumber);
        
        view.setCurrentResult(resultNumber);
    }
    
    /* Die gefragte und die eingegebene Zahl werden zwischengespeichert. Wenn sie gleich sind, wird im Controller eine neue Zufallszahl erzeugt, der interne computerScore geht um 1 nach oben, die Dualziffern werden auf 0 gesetzt und im View wird überprüft, ob der Nutzer die versteckte Funktion freigeschalten hat. */
    function onComputerButtonHacked() {
        var askedNum, actualNum, score;
        askedNum = document.getElementById("computer_question_number").innerHTML;
        actualNum = document.getElementById("computer_result").innerHTML;
        
        if (askedNum == actualNum){
            controller.handleMatchingNumbers();
            score = model.increaseComputerScore();
            view.resetBinaryDigits();
            view.displayHiddenText(score);
        }else{
            shake(document.getElementById("computer_hack"));
        }
    }
    
    /* Wenn die versteckte Computerfunktion gefunden wird (computerCount > 10), bekommt der Nutzer 30 Punkte. Der Score wird erhöht und gespeichert, anschließend wird der aktualisierte score geholt und im View angezeigt */
    function onHiddenComputerDiscovered() {
        var score;
        model.increaseScore(30);
        score = model.getUserScore();
        view.showNewUserScore(score);
    }
    
    
    // +++++ STUDIENLEISTUNG +++++
    /* Input und OutputContainer werden erzeugt; die Antwort wird zwischengespeichert und alles wird im View verarbeitet. Wenn die Eingabe valide ist, wird sie eingetragen, wenn nicht wird die CSShake Methode aufgerufen um einen visuellen Hinweis zu geben, dass die EIngabe nicht korrekt war */
    function onStudyAnswerButtonClicked(object) {
        var inputContainer, outputContainer, answerString, lastChar;
        lastChar = object.target.id.substr(object.target.id.length - 1);
        
        inputContainer = document.getElementById("input_study_" + lastChar);
        outputContainer = document.getElementById("answer_study_" + lastChar);
        
        answerString = inputContainer.value;
        
        view.writeStudyAnswer(outputContainer, answerString, object.target);
    }
    
    /* Diese Methode ist notwendig, da man die Studienleistung mehr als 1 mal abgeben kann. Beim erneuten Abgeben würden zusätzliche Punkte addiert, was falsch ist. Deshalb werden die erreichten Punkte abgezogen, um anschließend wieder welche zu verteilen */
    function onStudyPointsTakeBack(points) {
        var scorePoints, score;
        scorePoints = points.data;
        model.decreaseScore(scorePoints);
        score = model.getUserScore();
        document.getElementById("userScore").innerHTML = score.toString() + " Punkte";
    }
    
    
    // +++++ ÜBUNGSBLATT +++++
    /* Analog zur Studienleistung. Container erzeugt; Antwort an den View weiterleiten, der alles verarbeitet. */
    function onPaperAnswerButtonClicked(object) {
        var inputContainer, outputContainer, answerString, lastChar;
        lastChar = object.target.id.substr(object.target.id.length - 1);
        
        inputContainer = document.getElementById("input_paper_" + lastChar);
        outputContainer = document.getElementById("answer_paper_" + lastChar);
        
        answerString = inputContainer.value;
        
        view.writePaperAnswer(outputContainer, answerString, object.target);
    }
    
    /* Zählt für Studienleistung UND Übungsblatt. Wenn die Übung abgegeben und bewertet wurde, wird die Anzahl der erreichten Punkte dem gesamtScore hinzugefügt (Model) und im View angezeigt. */
    function onExercisePointsUpdated(points) {
        var scorePoints, score;
        scorePoints = points.data;
        model.increaseScore(scorePoints);
        score = model.getUserScore();
        view.showNewUserScore(score);
    }
    
    /* Zählt für Studienleistung UND Übungsblatt UND Computer. Versteckt die aktuelle Sicht und zeigt die Messages wieder an */
    function onBackButtonClicked() {
        view.hideExerciseDisplay();
    }
    
    
    // +++++ BÜRO +++++
    /* Wird aufgerufen, sobald der Nutzer das Büro betritt. In diesem Fall starten zwei Counter. Steps (10sek) zeigt nach vergangener Zeit die Nachricht an, dass sich Schritte nähern. Professor (20sek) nimmt einem Schlüssel und Spicker ab und verteilt zusätzlich 20 Minuspunkte */
    function onOfficeTimeStart() {
        var score;
        officeTimeoutSteps = setTimeout(function(){ 
            view.displaySystemMessage("Du hörst Schritte ... Sie kommen näher.") 
        }, 10000);
        
        officeTimeoutProfessor= setTimeout(function(){
            view.displaySystemMessage("Professor Proton: WAS MACHEN SIE IN MEINEM BÜRO?!?! Das kostet Sie 20 wertvolle Punkte!<br>Den Schlüssel nehme ich Ihnen natürlich ab und den Spickzettel werde ich auch wegpacken!");
            model.decreaseScore(20);
            score = model.getUserScore();
            document.getElementById("userScore").innerHTML = score.toString() + " Punkte";
            model.getObject("Schlüssel");
            office.removeObjectFromRoom();
        }, 20000);
    }
    
    /* Wenn der Nutzer das Büro rechtzeitig verlässt, werden die Timer gestoppt */
    function stopOfficeTimeouts() {
        clearTimeout(officeTimeoutSteps);
        clearTimeout(officeTimeoutProfessor);
    }
    
    
    // +++++ OBJEKTE +++++
    /* Der View zeigt das zu bearbeitende Objekt an */
    function onObjectEdit(editableObject) {
        view.editTask(editableObject.data);
    }

    /* Wird verwendet, um das Objekt im Model zu speichern */
    function onObjectTaken(takenObject) {
        model.addToInventory(takenObject.data);
    }
    
    
    that.init = init;
    return that;
}());