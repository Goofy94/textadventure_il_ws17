var App = App || {};
App.TextAdventurePerson = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        name,
        talk,
        objects = [],
        teachingBasic, teachingExamples, teachingTwoDigit, teachingQuantifiers, teachingSummary,
        teachingBinaryBasic, teachingBinarySystem,
        studyCount = 0,
        paperCount = 0,
        passCount = 0,
        studyPoints = 0,
        PAPER_ANSWERS = ["0b111", "0b1101", "2", "35"];
    
    
    // ++++++ HANDLE INITIALISIERUNG ++++++
    function init(infoObject) {
        name = infoObject.name;
        talk = infoObject.talk;
        objects = infoObject.objects;
        teachingBasic = infoObject.teachingBasic;
        teachingExamples = infoObject.teachingExamples;
        teachingTwoDigit = infoObject.teachingTwoDigit;
        teachingQuantifiers = infoObject.teachingQuantifiers;
        teachingSummary = infoObject.teachingSummary;
        teachingBinaryBasic = infoObject.teachingBinaryBasic;
        teachingBinarySystem = infoObject.teachingBinarySystem;
    }
    
    /* Wird dazu benötigt, um den Namen der Person zu bekommen */
    function getName() {
        return name;
    }
    
    
    
    // ++++++ HANDLE ACTIONS ++++++
    // ++ ABGEBEN ++
    function give(object) {
        var answers = [], exercise, resultString;
        exercise = object.name.toLowerCase();
        if(exercise === "studienleistung") {
            answers = getStudyAnswers();
            resultString = checkAnswers(answers, exercise);
            studyCount = studyCount + 1;
        } 
        else if(exercise === "übungsblatt") {
            answers = getPaperAnswers();
            resultString = checkAnswers(answers, exercise);
            paperCount = paperCount + 1;
        } 
        else if(exercise === "ausweis" && name==="Professor Proton") {
            resultString = "Vielen Dank. Da ist ja mein verschollener Ausweis. Dafür bekommen Sie einen zusätzlichen Versuch für Ihre Studienleistung. Machen Sie es gut.";
            passCount = passCount + 1;
            studyCount = studyCount - 1;
        } 
        else {
            resultString = "Danke. Mal sehen was ich damit anfangen kann.";
        }
        objects.push(object);
        
        return resultString;
    }
    
    // ++ NEHMEN ++
    function takeObject(sObject) {
        var tmpObject, i, objectName;
        sObject = sObject.toLowerCase();
        
        //Verhindert, dass die Studienleistung/Übung öfter als zwei mal abgegeben werden kann
        if(sObject === "studienleistung" && studyCount >= 2) {
            return "Du kannst deine Studienleistung nicht öfter nehmen und abgeben.";
        }
        if(sObject === "übungsblatt" && paperCount >= 1) {
            return "Du kannst dein Übungsblatt nicht öfter nehmen und abgeben.";
        }
        if(sObject === "ausweis" && passCount >= 1) {
            return "Du kannst mir meinen Ausweis nicht wieder wegnehmen.";
        }
        
        for(i = 0; i < objects.length; i++) {
            tmpObject = objects[i];
            objectName = tmpObject.name.toLowerCase();
            if(objectName===sObject){
                //remove object from array
                objects = removeObjectFromArray(objectName);
                that.notifyAll("objectTaken", tmpObject);
                if(objectName==="studienleistung") {
                    that.notifyAll("takeBackStudyPoints", studyPoints);
                    studyPoints = 0
                }
                return "Du nimmst " + capitalizeFirstLetter(objectName) + ".";
            }
        }
        return "Es befinden sich keine Objekte hier.";
    }
    
    // ++ FRAGEN ++
    function ask(sAskObject) {
        switch(sAskObject.toLowerCase()) {
            case "informationslinguistik":
                return teachingBasic;
                break;
            case "mehrstellig":
                return teachingTwoDigit;
                break;
            case "quantoren":
                return teachingQuantifiers; 
                break;
            case "zeichendefinitionen":
                return teachingExamples;
                break;
            case "zusammenfassung":
                return teachingSummary;
                break;
            case "dualsystem":
                return teachingBinaryBasic;
                break;
            case "beispiel":
                return teachingBinarySystem;
                break;
            default:
                return null;
        }
    }
    
    // ++ REDEN ++
    function talk(mensaCount) {        
        // Only here to get the Users to the Mensa
        if(name=="Dozent Dietrich" && mensaCount<=0) {
            return "Dozent Dietrich: Sie sehen ein wenig blass aus. Holen Sie sich am besten in der <b>Mensa</b> eine Stärkung und ruhen Sie sich ein wenig aus bevor Sie hier weitermachen."
        }
        
        if(objects.length !== 0) {
            return talk + "<br>Er hält etwas in der Hand: " + iterateArrayCreateAnswer(objects);
        } else {
            return talk;
        }
    }
    
    
    
    // ++++++ HANDLE STUDIENLEISTUNG/ÜBUNGSBLATT ++++++ 
    /* Diese Funktion holt sich die eingetragenen Antworten der Studienleistung. Output: Array */
    function getStudyAnswers() {
        var answers = [];
        for(var i=1; i<5; i++) {
            answers[i-1] = document.getElementById("answer_study_" + i).innerHTML;
        }
        return answers;
    }
    
    /* Diese Funktion holt sich die eingetragenen Antworten des Übungsblattes. Output: Array */
    function getPaperAnswers() {
        var answers = [];
        for(var i=1; i<5; i++) {
            answers[i-1] = document.getElementById("answer_paper_" + i).innerHTML;
        }
        return answers;
    }
    
    /* Diese Funktion prüft je nach Art der Übung (Studienleistung, Übungsblatt) die Antworten und vergibt die jeweiligen Punkte. Die Funktion liefert einen String zurück, der dem Nutzer angezeigt wird. Wie viele richtige Antowrten und Punkte kann man erkennen. */
    function checkAnswers(answers, exercise) {
        var userAnswer, actualAnswer, correctAnswers = 0, answerNumString = 0, answerObject = {}, correctPoints = 0, pointsNumString = 0, exercisePoints = 0;
        
        for(var j = 0; j < answers.length; j++) {
            userAnswer = answers[j];
            answerObject = checkStudyAnswerCorrect(userAnswer, j);
            
            if(exercise==="studienleistung") {
                correctAnswers = correctAnswers + answerObject.result;
                correctPoints = correctPoints + answerObject.points;
                studyPoints = correctPoints;
            } 
            else if(exercise==="übungsblatt") {
                actualAnswer = PAPER_ANSWERS[j];
                if(userAnswer === actualAnswer){
                    correctAnswers++;
                    correctPoints = correctPoints + 10;
                }
            }
        }
        
        answerNumString = correctAnswers;
        correctAnswers = 0;
        
        pointsNumString = correctPoints;
        exercisePoints = exercisePoints + correctPoints;
        correctPoints = 0;
        
        that.notifyAll("exercisePointsUpdated", exercisePoints);
        
        return "Du gibst " + exercise + " ab.<br>Du hast " + answerNumString.toString() + " von 4 Fragen richtig beantwortet und " + pointsNumString.toString() + " von 40 möglichen Punkten erzielt.";
    }
    
    /* NUR für die Studienleistung. Die Funktion prüft die Richtigkeit der Eingabe, da man RegEx auf verschiedene Weisen richtig angeben kann. Sie beziht verschiedene Faktoren ein wie z.B. Läge, Anzahl der Klammern, Ausgaben, usw. Es wird pro Aufgabe ein Score zurückgegeben. */
    function checkStudyAnswerCorrect(answer, index) {
        var string, patt, answerObject = {};
        
        string = "" + answer;
        patt = new RegExp(string);
        
        switch(index) {
            case 0:
                // Nummern von 3-8; [3-8]; Länge: 5
                var nofalseDigit = patt.test("0129");
                var noChar = patt.test("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
                if(noChar===false && nofalseDigit===false) {
                    answerObject.result = 1;
                    answerObject.points = checkPointsOfAnswer(string, 5);
                    return answerObject;
                } else {
                    answerObject.points = 0;
                    answerObject.result = 0;
                    return answerObject;
                }
                break;
                
            case 1:
                // Nummern von 0-9 außer die 2; [0-13-9]; Länge: 8
                var nofalseDigit = patt.test("2");
                var noChar = patt.test("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
                if(noChar===false && nofalseDigit===false) {
                    answerObject.result = 1;
                    answerObject.points = checkPointsOfAnswer(string, 8);
                    return answerObject;
                } else {
                    answerObject.points = 0;
                    answerObject.result = 0;
                    return answerObject;
                }
                break;
                
            case 2:
                // Unendlich lang, nur aus 1, 2 und 5; [125]+; Länge: 6
                var nofalseDigit = patt.test("0346789");
                var noChar = patt.test("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ");
                var plus = /\+/.test(string)
                if(noChar===false && nofalseDigit===false && plus===true) {
                    answerObject.result = 1;
                    answerObject.points = checkPointsOfAnswer(string, 6);
                    return answerObject;
                } else {
                    answerObject.points = 0;
                    answerObject.result = 0;
                    return answerObject;
                }
                break;
                
            case 3:
                // 1-99 plus abcd; [1-9][0-9]?[a-d]?; Länge: 17
                var noFalseChar = patt.test("efghijklmnopqrstuvwxyzEFGHIJKLMNOPQRSTUVWXYZ");
                var brackets = checkMultipleChars(string, "[", 3);
                var questionMark = checkMultipleChars(string, "?", 2);
                if(noFalseChar===false && questionMark===true && brackets===true) {
                    answerObject.result = 1;
                    answerObject.points = checkPointsOfAnswer(string, 17);
                    return answerObject;
                } else {
                    answerObject.points = 0;
                    answerObject.result = 0;
                    return answerObject;
                }
                break;
                
            default:
                return 0;
        }
    }
    
    /* Diese Funktion gibt je nach Länge des RegEx eine Punktzahl zurück. Der kürzestmögliche Ausdruck (der auch gelehrt wurde) wird als Parameter mitgegeben, so wie auch die tatsächliche EIngabe des Nutzers. */
    function checkPointsOfAnswer(answer, iShort) {
        if(answer.length<=iShort){
            return 10;
        } else if(answer.length>iShort && answer.length<=iShort+4) {
            return 8;
        } else {
            return 6;
        }
    }
    
    /* Hilfsfunktion, um zu überprüfen, ob z.B. genug Fragezeichen oder Klammern in der Nutzereingabe vorkommen. 
    Output: Boolean */
    function checkMultipleChars(string, char, count) {
        var appearance = 0;
        for(var i = 0; i < string.length; i++){
            if(string.charAt(i)===char){
                appearance++;
            }
        }
        if(appearance>=count){
            return true;
        } else {
            return false;
        }
    }
    
    /* Der studyCount wird dazu benutzt, um festzustellen, wie oft die Übung abgegeben wurde */
    function getStudyCount(){
        return studyCount;
    }
    
    /* Der paperCount wird dazu benutzt, um festzustellen, wie oft die Übung abgegeben wurde */
    function getPaperCount() {
        return paperCount;
    }
    
    
    
    // ++++++ HILFSFUNKTIONEN ++++++
    /* Wird verwendet, um einen dynamischen String zu erzeugen, was die Personen in ihrer Hand halten. Dazu wird das Array, die Objekte, iteriert und wenn ein Objekt vorhanden ist, der Name im String gespeichert. */
    function iterateArrayCreateAnswer(array) {
        var arrayObjects = "", counter = 0;
        
        array.forEach(function(element) {
            if (counter === 0) {
                arrayObjects = element.name;
                counter++;
            } else {
                arrayObjects = arrayObjects + ", " + element.name;
            }
        });
        
        return arrayObjects;
    }
    
    /* Input: String. Das Objekt mit dem Namen wird aus dem Array entfernt (z.B. bei der Aktion: nehmen) */
    function removeObjectFromArray(sObject) {
        var newObjects = [];
        newObjects = objects.filter(function(el) {
            return el.name !== capitalizeFirstLetter(sObject);
        });
        return newObjects;
    }
    
    /* Diese Funktion gibt einen String mit großgeschriebenen ersten Char zurück. Input: String */
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    
    
    that.getStudyCount = getStudyCount;
    that.getPaperCount = getPaperCount;
    that.ask = ask;
    that.give = give;
    that.getName = getName;
    that.takeObject = takeObject;
    that.talk = talk;
    that.init = init;
    return that;
};