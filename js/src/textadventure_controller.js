var App = App || {};
App.TextAdventureController = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        GREETINGS = ["Hallo!", "Hi", "Servus", "Hallöchen!", "Gott zum Gruße", "Grüß dich"],
        SMALLTALK = ["Das Wetter ist herrlich, oder? :)", "Alles klar!", "Alles gut! Was geht bei dir?"],
        UNDERSTANDINGPROBLEMS = ["Was hat das zu bedeuten?", "Das verstehe ich nicht.", "Ich weiß nicht was das heißt.", "Wie bitte?", "Diese Eingabe kann ich nicht verarbeiten."],
        SAYING = ["Alle Wege führen nach Rom? Wohl noch nie in eine Sackgasse gefahren?", "Gelfrisur und Polohemd - Ich bin BWL-Student.", "Heute studiert jeder zweite an der Uni sein späteres Hobby für die Arbeitslosigkeit.", "Ich brauche kein Internet. Das Internet braucht mich.", "<b>Wenn Albert albert, ruht Ruht. Wenn Ruht ruht, albert Albert.</b> (seltener Spruch)", "Wenn ich nicht gerade wach bin oder schlafe, bin ich eigentlich ganz erträglich.", "Student: Warum müssen wir immer Ihrer Meinung sein? Professor: Müssen Sie nicht, aber meine ist die richtige...", "Ich kann kochen, nur schmeckt es keinem.", "Lass den Kopf nicht hängen, das sieht bei dir richtig doof aus.", "Zuverlässigkeit hat einen Namen - meiner ist es nicht.", "Gestern hatte ich noch Geldsorgen, heute habe ich beschlossen, im Lotto zu gewinnen. Ganz einfach.", "Ich bin keinesfalls vergesslich. Jedenfalls nicht solange, bis mir entfallen ist, warum nicht.", "Wer anderen eine Grube graben will, beauftragt am besten einen Subunternehmer.", "Gefällt mir ist der kleine Bruder von doof.", "Warum stehen Studenten schon um halb sieben Uhr auf? Weil um acht der Supermarkt zu macht.", "Ich habe herausgefunden, dass Geld allein mich nicht glücklich macht - es muss mir auch gehören."],
        currentPlace,
        settingsStep = "1",
        professor, lecturer,
        campus, mensa, cip2, h6, pt, zhg, office,
        model,
        result = 0,
        falseInput = 0,
        gameEndCount = 0,
        lectureCount = 0,
        mensaCount = 0;

    // ++++++ INIT ELEMENTS ++++++ 
    function initDifferentRooms(campusClass, mensaClass, cip2Class, h6Class, ptClass, zhgClass, officeClass) {
        campus = campusClass;
        mensa = mensaClass;
        cip2 = cip2Class;
        h6 = h6Class;
        pt = ptClass;
        zhg = zhgClass;
        office = officeClass;
    }
    
    function initCharacters(professorClass, lecturerClass) {
        professor = professorClass;
        lecturer = lecturerClass;
    }
    
    function initModel(modelClass) {
        model = modelClass;
    }
    
    
    // ++++++ HANDLE MESSAGE ++++++ 
    /* Der aktuelle Ort wird gespeichert. Die Nachricht wird mit RegEx bearbeitet und es wird überprüft ob der Endzustand erreicht ist. */
    function handleNewMessage(messageString, sCurrentPlace) {
        currentPlace = sCurrentPlace;
        
        handleMessageRegEx(messageString);
        
        checkGameEnding(messageString);
    }
    
    /* Die Prioritäten sind: 1 Smalltalk, 2 Aktion, 3 Ort. Die Nachricht wird verarbeitet und anschließend an das Model weitergegeben und verarbeitet. */
    function handleMessageRegEx(messageString) {
        var answerObject = {}, sSmalltalkAnswer, sPlaceAnswer, sActionAnswer;
        
        /*Die Smalltalk Fähigkeiten stehen über der Suche nach Orten und Aktionen,
        wird smalltalk entdeckt, wird der String nicht weiter bearbeitet*/
        sSmalltalkAnswer = answerSmallTalk(messageString);
        if(sSmalltalkAnswer === null) {
            
            sActionAnswer = userAction(messageString);
            if(sActionAnswer === null) {
                
                sPlaceAnswer = goToPlace(messageString);
                if(sPlaceAnswer === null) {
                    
                    answerObject.message = messageString;
                    answerObject.answer = _.sample(UNDERSTANDINGPROBLEMS);
                    answerObject.place = currentPlace;
                    
                    // Wenn der User 6 mal hintereinander etwas nicht verständliches eingibt, dan kommt diese Hilfe
                    if (falseInput>4){
                        handleHelplessUser(messageString);
                        return;
                    } else { falseInput++; }
                } else {
                    answerObject.message = messageString;
                    answerObject.answer = sPlaceAnswer.message;
                    answerObject.place = sPlaceAnswer.place;
                }
            } else {
                answerObject.message = messageString;
                answerObject.answer = sActionAnswer;
                answerObject.place = currentPlace;
            }
        } else {
            answerObject.message = messageString;
            answerObject.answer = sSmalltalkAnswer;
            answerObject.place = currentPlace;
        }
        
        model.handleNewMessage(answerObject);
    }
    
    /* Diese kleine Hilfe wird dem Nutzer immer dann angezeigt, wenn er sechs Kommandos eingibt, die alle am Stück nicht vertanden werden. */
    function handleHelplessUser(message) {
        var answerObject = {};
        
        answerObject.message = message;
        answerObject.answer = "Hilfe bekommst du im linken Fenster nebenan (Kommandos, allg. Hilfekarte, Minimap).<br>Hast du schon versucht an einen anderen Ort zu <b>gehen</b> und dich dort umzusehen?<br>Kommandos werden am Besten verstanden wenn du zuerst das Objekt und anschließend die Aktion wählst, also z.B. Stift aufheben anstatt aufheben Stift.";
        answerObject.place = currentPlace;
            
        falseInput = 0;
        
        model.handleNewMessage(answerObject);
    }
    
    
    // ++++++ HANDLE SMALLTALK ++++++ 
    function answerSmallTalk(sMessage) {
        switch (true) {
            case /\bhallo+\b|\bhi(ho)?\b|\bser(vu)?s\b/gi.test(sMessage):
                var greeting = _.sample(GREETINGS);
                return greeting;
                break;
            case /\b(wie geht('?s|es( dir)?)\b|\bwas (l(ä|ae)uft\b|\bgeht( ab)?)\b|\balles (klar|roger|ok))\b/gi.test(sMessage):
                var smalltalk = _.sample(SMALLTALK);
                return smalltalk;
                break;
            default:
                return null;
                break;
        }
    }
    
    
    // ++++++ HANDLE ACTION ++++++ 
    /*Es wird immer nach bestimmten Stichwörtern gesucht, z.B. aufheben oder umsehen. Wird ein solches Stichwort gefunden, wird entweder ein Funktion aufgerufen oder es wird nach weiteres Stichwörtern gesucht. Meistens wird danach entschieden, für welchen Ort eine Aktion bestimmt ist, so z.B. umsehen -> Ort filtern -> Mensa, also Mensa umsehen. Es wird immer die passende Antwort zurückgegeben und evtl. eine Zusatzfunktion aufgerufen */
    function userAction(sMessage) {
        switch(true) {
                
            // UMSEHEN
            case /\bumsehen\b|\bsieh.*um\b/gi.test(sMessage):
                var placeAnswer;
                switch (currentPlace) {
                    case "campus":
                        placeAnswer = campus.look();
                        break;
                    case "mensa":
                        placeAnswer = mensa.look();
                        break;
                    case "cip2":
                        placeAnswer = cip2.look();
                        break;
                    case "h6":
                        placeAnswer = h6.look();
                        break;
                    case "pt":
                        placeAnswer = pt.look();
                        break;
                    case "zhg":
                        placeAnswer = zhg.look();
                        break;
                    case "büro":
                        placeAnswer = office.look();
                        break;
                    default:
                        return null;
                        break;
                }
            return placeAnswer;
                
                
            // AUFHEBEN
            case /\baufheben\b|\bheb \w+ auf\b/gi.test(sMessage):
                // Man muss den String durchsuchen, was der Nutzer aufheben möchte. Bsp: Stift
                var objectUserWantToTake, takeAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                
                if (/\baufheben\b/gi.test(sMessage)){
                    objectUserWantToTake = aMessageWords[0];
                } else if (/\bheb \w+ auf\b/gi.test(sMessage)) {
                    objectUserWantToTake = aMessageWords[1];
                }
                
                switch (currentPlace) {
                    case "campus":
                        takeAnswer = campus.takeObject(objectUserWantToTake);
                        break;
                    case "mensa":
                        takeAnswer = mensa.takeObject(objectUserWantToTake);
                        break;
                    case "cip2":
                        takeAnswer = cip2.takeObject(objectUserWantToTake);
                        break;
                    case "h6":
                        takeAnswer = h6.takeObject(objectUserWantToTake);
                        break;
                    case "pt":
                        takeAnswer = pt.takeObject(objectUserWantToTake);
                        break;
                    case "zhg":
                        takeAnswer = zhg.takeObject(objectUserWantToTake);
                        break;
                    case "büro":
                        takeAnswer = office.takeObject(objectUserWantToTake);
                        break;
                    default:
                        return null;
                        break;
                }
            return takeAnswer;
            
                
            // NEHMEN
            case /\bnehmen\b|\bnimm .+\b/gi.test(sMessage):
                // Man muss den String durchsuchen, was der Nutzer aufheben möchte. Bsp: Stift
                var objectUserWantToTake, takeAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                
                if (/\bnehmen\b/gi.test(sMessage)){
                    objectUserWantToTake = aMessageWords[0];
                } else if (/\bnimm .+\b/gi.test(sMessage)) {
                    objectUserWantToTake = aMessageWords[1];
                }
                
                switch (currentPlace) {
                    case "h6":
                        takeAnswer = professor.takeObject(objectUserWantToTake);
                        break;
                    case "cip2":
                        takeAnswer = lecturer.takeObject(objectUserWantToTake);
                        break;
                    default:
                        return "Hier ist keine Person von der du etwas nehmen könntest.<br>Möchtest du etwas vom Boden aufheben?";
                        break;
                }
            return takeAnswer;
                
            
            // ANSEHEN/UNTERSUCHEN
            case /\bansehen\b|\buntersuchen\b|\bsieh.*an\b|\buntersuche\b/gi.test(sMessage):
                var examineObject, examineAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                if (/\bansehen\b|\buntersuchen\b/gi.test(sMessage)){
                    examineObject = aMessageWords[0];
                } else if (/\bsieh.*an\b|\buntersuche\b/gi.test(sMessage)) {
                    examineObject = aMessageWords[1];
                }
                examineAnswer = model.examine(examineObject);
                return examineAnswer;
                
                
            // ESSEN
            case /\bessen\b|\biss\b/gi.test(sMessage):
                var eatObject, eatAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                if (/\bessen\b/gi.test(sMessage)){
                    eatObject = aMessageWords[0];
                } else if (/\biss\b/gi.test(sMessage)) {
                    eatObject = aMessageWords[1];
                }
                eatAnswer = model.eat(eatObject);
                return eatAnswer;
                
                
            // SPRECHEN/REDEN
            case /\bsprechen\b|\bsprich\b|\breden?\b|\bspreche\b/gi.test(sMessage):
                var talkAnswer;
                switch (currentPlace) {
                    case "cip2":
                        talkAnswer = lecturer.talk(mensaCount);
                        break;
                    case "h6":
                        talkAnswer = professor.talk();
                        break;
                    default:
                        talkAnswer = "Hier ist niemand mit dem du sprechen könntest."
                        break;
                }
                return talkAnswer;
            
                
            // WEGERFEN
            case /\bwegwerfen\b|\bwirf \w+ weg\b/gi.test(sMessage):
                var dropObject, dropAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                if (/\bwegwerfen\b/gi.test(sMessage)){
                    dropObject = aMessageWords[0];
                } else if (/\bwirf \w+ weg\b/gi.test(sMessage)) {
                    dropObject = aMessageWords[1];
                }
                if(model.userHasObject(dropObject)){
                    switch (currentPlace) {
                    case "campus":
                        dropAnswer = campus.drop(model.getObject(dropObject));
                        break;
                    case "mensa":
                        dropAnswer = mensa.drop(model.getObject(dropObject));
                        break;
                    case "cip2":
                        dropAnswer = cip2.drop(model.getObject(dropObject));
                        break;
                    case "h6":
                        dropAnswer = h6.drop(model.getObject(dropObject));
                        break;
                    case "pt":
                        dropAnswer = pt.drop(model.getObject(dropObject));
                        break;
                    case "zhg":
                        dropAnswer = zhg.drop(model.getObject(dropObject));
                        break;
                    case "büro":
                        dropAnswer = office.drop(model.getObject(dropObject));
                        break;
                    default:
                        return null;
                        break;
                    }
                } else {
                    dropAnswer = "Du besitzt dieses Objekt nicht.";
                }
                return dropAnswer;
                
                
            // BEARBEITEN
            case /\bbearbeiten\b|\bbearbeite\b/gi.test(sMessage):
                var editableObject, editableAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                if (/\bbearbeiten\b/gi.test(sMessage)){
                    editableObject = aMessageWords[0];
                } else if (/\bbearbeite\b/gi.test(sMessage)) {
                    editableObject = aMessageWords[1];
                }
                editableAnswer = model.edit(editableObject);
                return editableAnswer;
                
                
            // ABGEBEN
            case /\bgib .+ an\b|\b.* (ab)?geben\b/gi.test(sMessage):
                var giveObject, giveAnswer, aMessageWords;
                aMessageWords = sMessage.split(" ");
                if (/\bgib .+ an\b/gi.test(sMessage)){
                    giveObject = aMessageWords[1];
                } else if (/\b.* (ab)?geben\b/gi.test(sMessage)) {
                    giveObject = aMessageWords[0];
                }
                
                if(model.userHasObject(giveObject)){
                    switch (currentPlace) {
                    case "cip2":
                        giveAnswer = lecturer.give(model.getObject(giveObject));
                        break;
                    case "h6":
                        giveAnswer = professor.give(model.getObject(giveObject));
                        break;
                    default:
                        giveAnswer = "Hier ist niemand dem du etwas geben könntest.";
                        break;
                    }
                } else {
                    giveAnswer = "Du besitzt dieses Objekt nicht.";
                }
                return giveAnswer;
                
                
            // So viele Arten danach zu Fragen. Deshalb nur das Stichwort "Informationslinguistik"
            case /\binformationslinguistik\b|\bquantoren\b|\bmehrstellig\b|\bzeichendefinitionen\b|\bzusammenfassung\b/gi.test(sMessage):
                var askObject, askAnswer;
                switch (currentPlace) {
                    case "h6":
                        askAnswer = professor.ask(sMessage);
                        break;
                    default:
                        return null;
                        break;
                    }
                return askAnswer;
                
                
            // COMPUTER/BENUTZEN
            case /\bcomputer\b/gi.test(sMessage):
                var computerAnswer;
                computerAnswer = "Du setzt dich an den Computer...";
                switch (currentPlace) {
                    case "cip2":
                        that.notifyAll("computerBinaryStarted");
                        break;
                    }
                return computerAnswer;
                
                
            // MATHEMATISCHE GRUNDLAGEN
            case /\bdualsystem\b|\bbeispiel\b/gi.test(sMessage):
                var askObjectMath, askAnswerMath;
                switch (currentPlace) {
                    case "cip2":
                        askAnswerMath = lecturer.ask(sMessage);
                        break;
                    }
                return askAnswerMath;
                
                
            // ENTSPANNEN (Mensa)
            case /\bentspannen\b/gi.test(sMessage):
                var askAnswerRelax;
                switch (currentPlace) {
                    case "mensa":
                        askAnswerRelax = "Du kannst hier wundervoll entspannen und kommst zu neuen Kräften.";
                        break;
                    default:
                        return null;
                    }
                return askAnswerRelax;
                
                
            // STARTEN ERNEUT
            case /\bstarten\b/gi.test(sMessage):
                if(gameEndCount>0) {
                    that.notifyAll("restartGame");
                    return "Neustart";
                } else {
                    return null;
                }
                
                
            // INVENTAR
            case /\binventar\b|\btasche\b/gi.test(sMessage):
                var inventoryAnswer;
                inventoryAnswer = model.requestInventory();
                return inventoryAnswer;
            
            default:
                return null;
                break;
        }
    }
    
    
    // ++++++ HANDLE GOING ++++++ 
    /*Ortabfrage wird nur gemacht, wenn die Stichwörter gehen/rennen/etc. erkannt wurden.
    Abfrage zu den Aktionen erfolgt separat, da man nicht explizit sagen muss wo man sich umsehen möchte.
    Dafür ist ja der Kontext angegeben. Zusätzlich wird je nach Ort die Minimap vervollständigt */
    function goToPlace(sMessage) {
        if (/\b(geh|renn)(en|st|e)?\b/gi.test(sMessage)){
            var answer, answerWithMessageAndPlace={}, goToPlace, possiblePlaces=[];
            
            /*Die Switch-Case überprüft ob der RegEx Ausdruck in dem String gefunden wurde und gibt true/false zurück*/
            switch (true) {
            
            /*mensa*/
            case /\bmensa\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("mensa");
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "mensa"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = mensa.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_mensa").style.visibility = "visible";
                        // Um die Leute in die Mensa zu "Zwingen"
                        mensaCount++;
                        answerWithMessageAndPlace.message = "Du gehst in die Mensa.";
                        answerWithMessageAndPlace.place = "mensa"
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            /*zhg*/
            case /\bzhg\b|\b((zentrales? )?h(ö|(oe))rsaal geb(ä|(ae))ude)\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("zhg");
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "zhg"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = zhg.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_zhg").style.visibility = "visible"; 
                        answerWithMessageAndPlace.message = "Du gehst zum Zentralen Hörsaal Gebäude.";
                        answerWithMessageAndPlace.place = "zhg"
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            /*CIP 2*/
            case /\bcip ?2?\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("cip2");
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "cip2"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = h6.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_cip").style.visibility = "visible"; 
                        answerWithMessageAndPlace.message = "Du gehst in den CIP Pool 2.";
                        answerWithMessageAndPlace.place = "cip2"
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            /*Hörsaal 6*/
            case /\bh((ö|(oe))rsaal)? ?6?\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("h6");
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "h6"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = h6.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        var message, time={};
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_h6").style.visibility = "visible";
                        
                        time = model.getTime();
                        if(time.hour==8 && time.minute<=15 && lectureCount < 1) {
                            lectureCount++;
                            message = "Du gehst rechtzeitig zur Vorlesung in den Hörsaal 6."
                        } else if(time.hour==8 && time.minute>15 && lectureCount < 1 || time.hour>8 && lectureCount < 1) {
                            lectureCount++;
                            message = "Du bist zu spät... Das kostet dich 10 Punkte. Die Vorlesung hat bereits begonnen. Leise schleichst du dich in den Hörsaal 6.";
                            model.decreaseScore(10);
                        } else {
                            message = "Du gehst in den Hörsaal 6."
                        }
                        answerWithMessageAndPlace.message = message;
                        answerWithMessageAndPlace.place = "h6"
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            /*campus*/
            case /\bcampus\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("campus");
                if(answer!== null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "campus"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = campus.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        answerWithMessageAndPlace.message = "Du gehst auf den Campus.";
                        answerWithMessageAndPlace.place = "campus"
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                    
                }
                break
                
            /*pt*/
            case /\bpt\b|\bphilosophie\b|\btheologie\b/gi.test(sMessage):
                answer = checkUserCurrentPlace("pt");
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "pt"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = pt.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_pt").style.visibility = "visible"; 
                        answerWithMessageAndPlace.message = "Du gehst ins Philosophie und Theologie Gebäude.";
                        answerWithMessageAndPlace.place = "pt"
                        that.notifyAll("goPTstopTimer");
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            /*büro*/
            case /\bbüro\b/gi.test(sMessage):
                var userHasKey;
                answer = checkUserCurrentPlace("büro");
                userHasKey = model.userHasObject("Schlüssel");
                if(!userHasKey){
                    answerWithMessageAndPlace.message = "Die Tür zum Büro ist verschlossen.";
                    answerWithMessageAndPlace.place = currentPlace;
                    return answerWithMessageAndPlace;
                }
                if(answer!==null) {
                    answerWithMessageAndPlace.message = answer;
                    answerWithMessageAndPlace.place = "büro"
                    return answerWithMessageAndPlace;
                } else {
                    possiblePlaces = findReachablePlaces();
                    goToPlace = office.checkIfPlaceIsReachable(sMessage, possiblePlaces);
                    
                    if(goToPlace){
                        // Display the MAP Element in the Help Content
                        document.getElementById("map_office").style.visibility = "visible"; 
                        answerWithMessageAndPlace.message = "Du sperrst die Tür auf und gehst ins Büro des Professors.";
                        answerWithMessageAndPlace.place = "büro"
                        
                        //Countdown wird gestartet
                        that.notifyAll("officeTimeStart");
                        
                        return answerWithMessageAndPlace;
                    }else{
                        answerWithMessageAndPlace.message = "Dieser Ort ist von hier aus nicht erreichbar.";
                        answerWithMessageAndPlace.place = currentPlace;
                        return answerWithMessageAndPlace;
                    }
                }
                break;
                    
            default:
                answerWithMessageAndPlace.message = "Ich verstehe 'gehen', aber bitte gib das Kommando folgendermaßen: ORT gehen.";
                answerWithMessageAndPlace.place = currentPlace;
                return answerWithMessageAndPlace;
                break;
            }
        } else {
            return null;
        }
    }
    
    /* Diese Funktion überprüft, ob der Nutzer bereits an dem Ort ist, an welchen er gehen möchte */
    function checkUserCurrentPlace(sPlace) {
        if(currentPlace===sPlace) {
            return "Du befindest dich bereits hier: " + sPlace;
        } else {
            return null;
        }
    }
    
    /* Diese Funktion überprüft, ob der eingegebene Ort vom momentanen Standpunkt des Nutzers erreichbar ist 
    Es wird ein Array mit erreichbaren Orten zurückgegeben. */
    function findReachablePlaces() {
        switch(currentPlace){
            case "campus":
                return campus.getPlaces();
                break;
            case "mensa":
                return mensa.getPlaces();
                break;
            case "cip2":
                return cip2.getPlaces();
                break;
            case "h6":
                return h6.getPlaces();
                break;
            case "pt":
                return pt.getPlaces();
                break;
            case "zhg":
                return zhg.getPlaces();
                break;
            case "büro":
                return office.getPlaces();
                break;
        }
    }
    

    // ++++++ HANDLE SETTINGS ++++++ 
    /* Je nach Step (1, 2, 3), wird der Nutzer nach seinem Namen gefragt und das Spiel gestartet */
    function handleSettings(sMessage) {
        switch (settingsStep) {
            case "1":
                // Länge abfragen max. 15 Zeichen TODO
                if(sMessage.length < 15) {
                    that.notifyAll("updateModelWithUsername", sMessage);
                    settingsStep = "2";
                    break;
                } else {
                    break;
                }
            case "2":
                if(/\bj(a)?\b/gi.test(sMessage)) {
                    that.notifyAll("acceptModelWithUsername", sMessage);
                    settingsStep = "3";
                } else if (/\bn(ein)?\b/gi.test(sMessage)) {
                    that.notifyAll("declineModelWithUsername", sMessage);
                    settingsStep = "1";
                }
                break;
            case "3":
                if(/\bstart\b/gi.test(sMessage)) {
                    that.notifyAll("startTheGame", sMessage);
                }
                break;
            default:
                return null;
                break;
        }
    }
    
    
    // ++++++ HANDLE COMPUTER BINARY GAME ++++++ 
    /* Diese Funktion holt sich die Dualzahl und wandelt sie in eine Dezimalzahl um. Diese wird zurückgegeben */
    function handleDigitClick() {
        var com64, com32, com16, com8, com4, com2, com1, binaryNumber, decimalNumber;
        com64 = document.getElementById("computer_64").innerHTML;
        com32 = document.getElementById("computer_32").innerHTML;
        com16 = document.getElementById("computer_16").innerHTML;
        com8 = document.getElementById("computer_8").innerHTML;
        com4 = document.getElementById("computer_4").innerHTML;
        com2 = document.getElementById("computer_2").innerHTML;
        com1 = document.getElementById("computer_1").innerHTML;
        
        binaryNumber = com64 + com32 + com16 + com8 + com4 + com2 + com1;
        
        decimalNumber = parseInt(binaryNumber, 2);
        
        return decimalNumber;
    }
    
    /* Diese Funktion erzeugt eine Zufallszahl zwischen 1 und 127 und setzt sie auch in dem Container */
    function handleMatchingNumbers() {
        var randomNumber, questionNumberContainer;
        questionNumberContainer = document.getElementById("computer_question_number");
        
        randomNumber = Math.floor((Math.random() * 127) + 1);
        
        questionNumberContainer.innerHTML = randomNumber;
    }
    
    
    // ++++++ HANDLE GAME ENDING ++++++ 
    /* Diese Funktion überprüft, ob der Endzustand des Spiels erreicht wurde. Dieser ist erreicht, wenn beide Arbeiten (Studienleistung und Übungsblatt) mindestens 1-mal abgegeben wurden. Danach wird der Schlusstext ausgegeben und je nach erreichter Punktzahl auch einige lustige/coole Sprüche. Diese werden ebenfalls zufallsbasiert ausgewählt, um den Spieler zum erneuten durchspielen zu verleiten */
    function checkGameEnding(messageString) {
        var studyCount, paperCount, score, answerObject={}, sayingObject={}, numberSayings="", commandCount;
        studyCount = professor.getStudyCount();
        paperCount = lecturer.getPaperCount();
        score = model.getUserScore();
        commandCount = model.getCommandCount();

        if(paperCount >=1 && studyCount >= 1 && gameEndCount < 1) {
            answerObject.message = messageString;
            sayingObject = getSayings(score);
            
            if(sayingObject.number==1) {
                numberSayings = " neuen Spruch";
            } else {
                numberSayings = " neue Sprüche";
            }
            
            answerObject.answer = "<b>HERZLICHEN GLÜCKWUNSCH!</b><br>Du hast das TextadventURe erfolgreich beendet und " + score.toString() + " von 120 möglichen Punkten erreicht. Dadurch hast du dir " + sayingObject.number.toString() + numberSayings + " von insgesamt 16 Sprüchen erspielt:" + sayingObject.string + "<br><br>Du hast " + commandCount.toString() + " Kommandos benötigt. Versuch doch einmal diese Zahl zu toppen.<br>Du kannst das Spiel entweder erneut <b>starten</b> und weitere Sprüche freischalten oder weiterspielen und versuchen die komplette Universiät zu erkunden. Es gibt einige versteckte Orte und Aktionen, die du vielleicht noch nicht entdeckt hast.<br>Vielen Dank fürs Durchspielen.";
            answerObject.place = currentPlace;
            model.handleNewMessage(answerObject);
            gameEndCount++
        }
    }
    
    /* Diese Funktion liefert je nach Score eine unterschiedliche und randomisierte Anzahl von Sprüchen zurück, die der Spieler bekommt, wenn er das Spiel beendet hat. */
    function getSayings(score) {
        var number = 0, random, answerObject={}, sayingString="", randomArray=[];
        if(score===120) {
            number = 4;
        } else if(score>=100 && score <120) {
            number = 3;
        } else if(score>=80 && score <100) {
            number = 2;
        } else {
            number = 1;
        }

        for(var i=0; i<number; i++) {
            random = Math.floor((Math.random() * 16) + 0);
            randomArray[i] = random;
            sayingString = sayingString + "<br>&bull; " + SAYING[random];
        }
        
        answerObject.string = sayingString;
        answerObject.number = number;
        
        return answerObject;
    }
    
    
    that.handleMatchingNumbers = handleMatchingNumbers;
    that.handleDigitClick = handleDigitClick;
    that.initCharacters = initCharacters;
    that.initDifferentRooms = initDifferentRooms;
    that.initModel = initModel;
    that.handleNewMessage = handleNewMessage;
    that.handleSettings = handleSettings;
    return that;
};