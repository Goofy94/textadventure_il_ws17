var App = App || {};
App.TextAdventureModel = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        currentGameState = {
            place: "campus"
        },
        messageChronic = [],
        inventory = [],
        time = 0,
        hour = 8,
        score = 0,
        computerResult = 0,
        computerScore = 0,
        commandCount = 0;
    
    // +++++ HANDLE NEW MESSAGE +++++
    /* Hier wird die Eingabe, die Antwort, Ort, Zeit und der Score in einem GameObject gespeichert, sodass man jederzeit Zugriff auf den aktuellen gameState hat. */
    function handleNewMessage(keywordObject) {
        var message, answer, place, sTime, sScore;
        
        message = keywordObject.message;
        answer = keywordObject.answer;
        place = keywordObject.place;

        messageChronic.push({
            key: message,
            value: answer
        });
        
        sTime = increaseTimeByOneMinute();
        sScore = score.toString() + " Punkte";
        
        currentGameState = {
            place: place,
            time: sTime,
            score: sScore,
            messages: messageChronic
        };

        // Counter, der die gesamte Anzahl an Schritten des Nutzers ausgibt.
        commandCount++;
        
        that.notifyAll("currentGameStateUpdated", currentGameState);
    }
    
    /* Hilfsmethode: handleNewMessage(keywordObject)
    Die Zeit wird bei jeder Eingabe um eine Minute erhöht. Das soll dem Nutzer besser in den Bann ziehen und gleichzeitig etwas interaktiver und realer wirken. */
    function increaseTimeByOneMinute() {
        if(hour<=9){
            if(time<9){
                time++;
                return "0" + hour.toString() + ":0" + time.toString() + " Uhr";
            } else if (time < 59 && time >= 9) {
                time++;
                return "0" + hour.toString() + ":" + time.toString() + " Uhr";
            } else if (time >= 59) {
                time = 0;
                hour++;
                return hour.toString() + ":0" + time.toString() + " Uhr";
            }
        } else {
            if(time<9){
                time++;
                return hour.toString() + ":0" + time.toString() + " Uhr";
            } else if (time < 59 && time >= 9) {
                time++;
                return hour.toString() + ":" + time.toString() + " Uhr";
            } else if (time >= 59) {
                time = 0;
                hour++;
                return hour.toString() + ":0" + time.toString() + " Uhr";
            }
        }
    }
    
    
    // +++++ HANDLE SETTINGS +++++
    /* Mit dieser Methode wird der Nutzername und der Startort definiert und initialisiert */
    function saveUsername(sUsername) {
        currentGameState = {
            name: sUsername,
            place: "campus"
        };
    }
    
    
    // +++++ HANDLE ACTIONS +++++
    // ++ INVENTAR ++
    function requestInventory() {
        var inventoryAnswer="", counter=0;
        inventory.forEach(function(element) {
            if (counter === 0) {
                inventoryAnswer = inventoryAnswer + element.name;
                counter++;
            } else{
                inventoryAnswer = inventoryAnswer + ", " + element.name;
            }
        });
        if(inventoryAnswer === "") {
            return "Deine Tasche ist leer.";
        } else {
            return "In deiner Tasche befindet sich etwas: " + inventoryAnswer;
        }
    }
    
    /* Diese Funktion fügt dem Inventar ein neues Objekt hinzu. Input: Objekt */
    function addToInventory(object) {
        inventory.push(object);
    }
    
    // ++ ESSEN ++
    function eat(sObject) {
        var objectName, isIncluded=false, answer;
        inventory.forEach(function(element) {
            sObject = sObject.toLowerCase();
            objectName = element.name.toLowerCase();
            if(sObject === objectName) {
                isIncluded = true;
                if(element.edible){
                    answer = "Du isst " + capitalizeFirstLetter(sObject) + ". Mhhhhhhhh Sehr lecker. Das gibt 10 Punkte.";
                    inventory = removeObjectFromArray(sObject);
                    score = score + 10;
                } else {
                    answer = "Pfui. Das kann man nicht essen!";
                }
            }
        });
        //Hanlde if nothing is in the inventory
        if(!isIncluded) {
            return "Das Objekt befindet sich nicht in deinem Inventar";
        } else {
            return answer;
        }
    }
    
    // ++ UNTERSUCHEN ++
    function examine(sObject) {
        var objectName, isIncluded=false, answer;
        inventory.forEach(function(element) {
            sObject = sObject.toLowerCase();
            objectName = element.name.toLowerCase();
            if(sObject === objectName) {
                isIncluded = true;
                answer = element.description;
            }
        });
        //Hanlde if nothing is in the inventory
        if(!isIncluded) {
            return "Das Objekt befindet sich nicht in deinem Inventar";
        } else {
            return answer;
        }
    }
    
    // ++ BEARBEITEN ++
    function edit(sObject) {
        var objectName, answer = "";
        inventory.forEach(function(element) {
            sObject = sObject.toLowerCase();
            objectName = element.name.toLowerCase();
            if(sObject === objectName && element.editable === true && userHasObject("stift")) {
                that.notifyAll("objectEdit", element);
                answer = "Du bearbeitest " + capitalizeFirstLetter(objectName) + ".";
            } 
            else if (sObject === objectName && element.editable === true && !userHasObject("stift")) {
                answer = "Du besitzt keinerlei Schreibgerät um die Aufgaben zu bearbeiten.";
            } 
            else if (sObject === objectName && element.editable === false) {
                answer = "Du kannst " + capitalizeFirstLetter(objectName) + " nicht bearbeiten.";
            }
        });
        if (answer === "") {
            answer = "Du besitzt dieses Objekt nicht.";
        }
        return answer;
    }
    
    // ++ WEGERFEN/ABGEBEN ++
    /* Diese Funktion sucht das Objekt im Inventar, entfernt es daraus und gibt es zurück */
    function getObject(sObject) {
        sObject = capitalizeFirstLetter(sObject.toLowerCase());
        var foundObject = _.findWhere(inventory, {name: sObject});
        inventory = removeObjectFromArray(sObject);
        return foundObject;
    }
    
    
    // +++++ HANDLE COMPUTER BINARY GAME +++++
    /* Diese Funktion zählt den internen Computerscore hoch, der für den versteckten Text zuständig ist */
    function increaseComputerScore() {
        computerScore = computerScore + 1;
        return computerScore;
    }
    
    /* Diese Funktion gibt das Ergebnis des Computers zurück */
    function getCurrentResult() {
        return computerResult;
    }
    
    /* Diese Funktion setzt das Ergebnis des Computers */
    function storeCurrentResult(result) {
        computerResult = result;
    }
    
    
    // ++++++ GETTER UND SETTER METHODEN ++++++
    // ++ Aktueller Ort ++
    function getCurrentPlace() {
        return currentGameState.place;
    }
    
    // ++ Aktueller Name ++
    function getCurrentUsername() {
        return currentGameState.name;
    }
    
    // ++ Aktueller Score ++
    function getUserScore() {
        return score;
    }
    
    // ++ Aktuelle Zeit ++
    function getTime() {
        var currentTime = {
            hour: hour,
            minute: time
        }
        return currentTime;
    }
    
    // ++ Gesamt Count aller Nutzereingaben ++ Idealfall=25 (mit Erläuterungen, 80Pkt.)
    function getCommandCount() {
        return commandCount;
    }
    
    
    // ++++++ HILFSMETHODEN ++++++
    /* Diese Funktion überprüft, ob der Nutzer ein Objekt in seinem Inventar hat. Input: String */
    function userHasObject(sObject) {
        var boolAnswer=false, objectName;
        inventory.forEach(function(element) {
            sObject = sObject.toLowerCase();
            objectName = element.name.toLowerCase();
            if(sObject === objectName) {
                boolAnswer = true;
            }
        });
        return boolAnswer;
    }
    
    /* Diese Funktion gibt einen String mit großgeschriebenen ersten Char zurück. Input: String */
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
    
    /* Diese Funktion entfernt ein Objekt aus dem Inventar. Input: String */
    function removeObjectFromArray(sObject) {
        var newObjects = [];
        newObjects = inventory.filter(function(el) {
            return el.name.toLowerCase() !== sObject.toLowerCase();
        });
        return newObjects;
    }
    
    
    // ++++++ HANDLE SCORE ++++++
    function increaseScore(points) {
        score = score + points;
    }
    
    function decreaseScore(points) {
        score = score - points;
    }
    
    
    that.getCommandCount = getCommandCount;
    that.getTime = getTime;
    that.decreaseScore = decreaseScore;
    that.getUserScore = getUserScore;
    that.increaseScore = increaseScore;
    that.increaseComputerScore = increaseComputerScore;
    that.storeCurrentResult = storeCurrentResult;
    that.getCurrentResult = getCurrentResult;
    that.edit = edit;
    that.eat = eat;
    that.handleNewMessage = handleNewMessage;
    that.getCurrentPlace = getCurrentPlace;
    that.saveUsername = saveUsername;
    that.getCurrentUsername = getCurrentUsername;
    that.addToInventory = addToInventory;
    that.requestInventory = requestInventory;
    that.examine = examine;
    that.userHasObject = userHasObject;
    that.getObject = getObject;
    return that;
};