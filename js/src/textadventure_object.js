var App = App || {};
App.TextAdventureObject = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        name,
        edible,
        description,
        editable;
    
    function init(infoObject) {
        name = infoObject.name;
        edible = infoObject.edible;
        description = infoObject.description;
        editable = infoObject.editable;
    }
    
    function getProperties() {
        var properties = {
            edible,
            name,
            description,
            editable
        };
        return properties;
    }
    
    that.init = init;
    that.getProperties = getProperties;
    return that;
};