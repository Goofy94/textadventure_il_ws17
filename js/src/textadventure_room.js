var App = App || {};
App.TextAdventureRoom = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        name,
        objects = [],
        places = [],
        persons = [],
        description;
    
    // ++++++ HANDLE INIT ++++++
    function init(infoObject) {
        name = infoObject.name;
        objects = infoObject.objects;
        places = infoObject.places;
        persons = infoObject.persons;
        description = infoObject.description;
    }
    
    /* GETTER METHODEN (INIT)*/
    function getPlaces() {
        return places;
    }
    
    function getName() {
        return name;
    }
    
    
    
    // ++++++ HANLDE ACTIONS ++++++
    // ++ UMSEHEN ++
    function look() {
        var answer="", currentAnswer="", objectString = "", personString = "", counter = 0;
        
        objectString = iterateArrayCreateAnswer(objects);
        personString = iterateArrayCreateAnswer(persons);
        if(personString !== ""){
            personString = "<br>" + personString + " steht herum.";
        }
        if(objectString !== ""){
            objectString = "<br>Auf dem Boden liegt etwas herum: " + objectString;
        }
        currentAnswer = personString + objectString;
        
        if(currentAnswer !== "") {
            answer = description + currentAnswer;
        } else{
            answer = description + "<br>Es befindet sich nichts Interessantes hier."
        }
        
        return answer;
    }
    
    // ++ NEHMEN ++
    function takeObject(sObject) {
        var tmpObject, i, objectName;
        sObject = sObject.toLowerCase();
        for(i = 0; i < objects.length; i++) {
            tmpObject = objects[i];
            objectName = tmpObject.name.toLowerCase();
            if(objectName===sObject){
                //remove object from array
                objects = removeObjectFromArray(objectName);
                that.notifyAll("objectTaken", tmpObject);
                return "Du hebst " + capitalizeFirstLetter(objectName) + " auf.";
            }
        }
        return "Dieses Objekt befindet sich nicht hier.";
    }
    
    // ++ WEGWERFEN
    function drop(object) {
        objects.push(object);
        return "Du wirfst " + capitalizeFirstLetter(object.name) + " auf den Boden.";
    }
    
    // ++ GEHEN ++
    /* Wenn der Nutzer wo hingehen möchten, dann wird hier geprüft, ob der besagte Ort auch erreichbar ist */
    function checkIfPlaceIsReachable(sMessage, arrPlaces) {
        var words = [], bCheck = false;
        words = sMessage.split(" ");
        words.forEach(function(element) {
            element = element.toLowerCase();
            if(arrPlaces.includes(element)){
                bCheck = true;
            }
        });
        if(bCheck===true){
            return true;
        }else {
            return false;
        }
    }
    
    // ++ ENTFERNEN, nur im BÜRO ++
    /* Wenn der Professor den Spieler im Büro erwischt, dann wird der Spickzettel vernichtet, solange er auf dem Boden liegt */
    function removeObjectFromRoom() {
        objects = [];
    }
    
    
    
    // ++++++ HILFSMETHODEN ++++++
    /* Input: String. Das Objekt mit dem Namen wird aus dem Array entfernt (z.B. bei der Aktion: nehmen) */
    function removeObjectFromArray(sObject) {
        var newObjects = [];
        newObjects = objects.filter(function(el) {
            return el.name !== capitalizeFirstLetter(sObject);
        });
        return newObjects;
    }
    
    /* Diese Funktion gibt einen String mit großgeschriebenen ersten Char zurück. Input: String */
    function capitalizeFirstLetter(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }
        
    /* Wird verwendet, um einen dynamischen String zu erzeugen, was in einem Raum vorhanden ist und welche Personen herumstehen. Dazu wird das Array, die Objekte, iteriert und wenn ein Objekt vorhanden ist, der Name im String gespeichert. */
    function iterateArrayCreateAnswer(array) {
        var arrayObjects = "", counter = 0;
        
        array.forEach(function(element) {
            if (counter === 0) {
                if(element.name !== undefined) {
                    arrayObjects = element.name;
                } else {
                    arrayObjects = element.getName();
                }
                counter++;
            } else {
                arrayObjects = arrayObjects + ", " + element.name;
            }
        });
        
        return arrayObjects;
    }
    
    
    
    that.removeObjectFromRoom = removeObjectFromRoom;
    that.getName = getName;
    that.look = look;
    that.takeObject = takeObject;
    that.init = init;
    that.checkIfPlaceIsReachable = checkIfPlaceIsReachable;
    that.getPlaces = getPlaces;
    that.drop = drop;
    return that;
};