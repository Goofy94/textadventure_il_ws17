var App = App || {};
App.TextAdventureView = function() {
    "use strict";
    /* eslint-env browser */
    /* global EventPublisher, _ */
    
    var that = new EventPublisher(),
        username,
        SCORE_HIGH = 10,
        scorePoints = 0;
    
    
    // ++++++ HANDLE NEW MESSAGE ++++++
    /* Hier wird der Score, die Zeit und der Ort angezeigt, anschließend wird die Nachricht weiter verarbeitet in der Funktion 'putTheNewMessageIntoTheList()' */
    function showUpdatedGameState(gameStateObject) {
        var sPlace;
        switch (gameStateObject.place) {
            case "campus":
                sPlace = "Campus";
                break;
            case "mensa":
                sPlace = "Mensa";
                break;
            case "cip2":
                sPlace = "CIP Pool 2";
                break;
            case "h6":
                sPlace = "Hörsaal 6";
                break;
            case "pt":
                sPlace = "Philosophie und Theologie (PT)";
                break;
            case "zhg":
                sPlace = "Zentrales Hörsaal Gebäude (ZHG)";
                break;
            case "büro":
                sPlace = "Büro des Professors";
                break;
            default:
                return null;
                break;
        }
        
        document.getElementById("currentPlace").innerHTML = sPlace;
        document.getElementById("currentTime").innerHTML = gameStateObject.time;
        document.getElementById("userScore").innerHTML = gameStateObject.score;
        
        putTheNewMessageIntoTheList(gameStateObject);
    }
    
    /* Mit Hilfe der letzten Eingabe und dem generierten Output wird ein Template Objekt erstellt und dem Nutzer angezeigt. Eine für seinen Namen + Nachricht und eines für die Antwort des Systems. */
    function putTheNewMessageIntoTheList(oGameState) {
        var entryObjectMessage, entryObjectAnswer, lastMessage, lastAnswer;
        
        lastMessage = oGameState.messages[oGameState.messages.length - 1].key;
        lastAnswer = oGameState.messages[oGameState.messages.length - 1].value;

        entryObjectMessage = {
            username: username,
            message: lastMessage
        };
        
        entryObjectAnswer = {
            username: "> ",
            message: lastAnswer
        };
        
        /* Da es nur 1 Video gibt, kann man es hier statisch erzeugen */
        var entryObjectVideo = {
            username: "",
            videosrc: "res/videos/Imagefilm_UniRegensburg.mp4",
            videoimage: "res/images/uni_regensburg.jpeg",
            videotext: "Imagefilm der Uni Regensburg"
        };
        
        if(oGameState.place === "mensa" && /entspannen/gi.test(lastMessage)) {
            createTemplateObject(entryObjectMessage);
            createTemplateObject(entryObjectAnswer);
            createTemplateObjectVideo(entryObjectVideo);
        } else {
            /* This is the Input/Question/etc. from the user*/
            createTemplateObject(entryObjectMessage);
            /* That is the Answer the computer gives you ased on the current Answer */
            createTemplateObject(entryObjectAnswer);
        }
    }
    
    /* Diese Funktion schreibt eine Nachricht, ohne Nutzereingabe, beispielsweise wenn der Count abläuft (Büro) oder bevor das Spiel startet */
    function displaySystemMessage(sDefaultMessage) {
        var entryDefaultObject;
        
        entryDefaultObject = {
            username: "> ",
            message: sDefaultMessage
        };
        
        createTemplateObject(entryDefaultObject);
    }
    
    // ++ HANDLE TEMPLATES ++
    function createTemplateObjectVideo(entryObjectMessage) {
        var templateListEntryString, listEntryString, createView, messageBoardList, entry;
        
        /* Here you first get the list element and the template list element that will be created here */
        messageBoardList = document.getElementById("messageBoard");
        templateListEntryString = document.getElementById("videoMessageScript").innerHTML;
        
        createView = _.template(templateListEntryString);
        listEntryString = createView(entryObjectMessage);
        entry = document.createElement("li");
        entry.className = "gameMessange";
        entry.innerHTML = listEntryString;
        messageBoardList.appendChild(entry);
        entry.scrollIntoView();
    }
    
    function createTemplateObject(entryObjectMessage) {
        var templateListEntryString, listEntryString, createView, messageBoardList, entry;
        
        /* Here you first get the list element and the template list element that will be created */
        messageBoardList = document.getElementById("messageBoard");
        templateListEntryString = document.getElementById("gameMessageScript").innerHTML;
        
        createView = _.template(templateListEntryString);
        listEntryString = createView(entryObjectMessage);
        entry = document.createElement("li");
        entry.className = "gameMessange";
        entry.innerHTML = listEntryString;
        messageBoardList.appendChild(entry);
        entry.scrollIntoView();
    }
    
    // ++ HANDLE SETTINGS ++
    /* Nutzername wird abgespeichert, ggf. nachgefragt */
    function saveUsername(sUsername) {
        username = sUsername;
        document.getElementById("userName").innerHTML = sUsername;
        
        displaySystemMessage("Möchtest du '" + sUsername + "' genannt werden? (ja / nein)");
    }
    
    /* Der Starttext wird angezeigt und das eigentliche Spiel gestartet. */
    function clearConsoleAndStartGame() {
        var messageBoardList = document.getElementById("messageBoard").innerHTML = "";
        displaySystemMessage("Heute ist dein erster Tag an der Universität Regensburg. <br> Es ist 08:00 Uhr morgens und du stehst inmitten von vielen Studenten auf dem Campus. In deiner Tasche befindet sich dein Stu... OH NEIN!, du hast deinen Stundenplan wohl auf dem Campus verloren. Du musst ihn finden, da du auf ihm Ort und Zeit deiner Vorlesungen finden kannst. <br> Vorallem am ersten Tag ist es wichtig in alle Vorlesungen und Seminare rechtzeitig zu kommen, da hier oft wichtige organisatorische Dinge und die Inhalte des Kurses besprochen werden. Sieh also zu, dass du pünktlich zu deinen Vorlesungen erscheinst.")
    }
    
    
    
    // ++++++ HANDLE STUDIENLEISTUNG und ÜBUNGSBLATT ++++++
    /* Wenn Studienleistung oder Übungsblatt bearbeitet werden, so werden sie hier angezeigt und das Messageboard versteckt */
    function editTask(sEditObject) {
        var objectName;
        objectName = sEditObject.name;
        switch(objectName){
            case "Studienleistung":
                document.getElementById("messageBoard").style.display = "none";
                document.getElementById("exercise").style.display = "block";
                disableInput();
                break;
            case "Übungsblatt":
                document.getElementById("messageBoard").style.display = "none";
                document.getElementById("paper").style.display = "block";
                disableInput();
                break;
        }
    }
    
    /* Diese Funktion ist für alle drei Übungen (StdLstg, ÜbngBl, Comp) gedacht. Alle anderen Displays werden versteckt und das Messageboard angezeigt. */
    function hideExerciseDisplay() {
        document.getElementById("messageBoard").style.display = "block";
        document.getElementById("exercise").style.display = "none";
        document.getElementById("paper").style.display = "none";
        document.getElementById("computer").style.display = "none";
        document.getElementById("playground_shadow").style.backgroundColor = "";
        document.getElementById("playground_shadow").classList.add("shadow");
        var messages = document.querySelectorAll(".gameMessange");
        messages[messages.length-1].scrollIntoView();
        enableInput();
    }
    
    /* Wenn die Nutzereingabe in einem gültigen Rahmen liegt, wird die Antwort eingetragen in den vorgesehen Container */
    function writeStudyAnswer(outputContainer, answerString, target) {
        if(/[0-9a-zA-Z\[\]]/gi.test(answerString)){
            outputContainer.innerHTML = answerString;
        } else {
            shake(target);
        }
    }
    
    /* Wenn die Nutzereingabe in einem gültigen Rahmen liegt, wird die Antwort eingetragen in den vorgesehen Container */
    function writePaperAnswer(outputContainer, answerString, target) {
        if(/^[0-9]+$/.test(answerString)){
            if(/[12]+/gi.test(outputContainer.id)) {
                outputContainer.innerHTML = "0b" + answerString;
            } else {
                outputContainer.innerHTML = answerString;
            }
        } else {
            shake(target);
        }
        
        
    }
    
    /* Während man eine Aufgabe bearbeitet, wird das Eingabefeld und der Button deaktiviert */
    function disableInput() {
        document.getElementById("messageInput").disabled = true;
        document.getElementById("clickMeButton").disabled = true;
    }
    
    /* Hier werden Eingabefeld und Button wieder aktiviert */
    function enableInput() {
        document.getElementById("messageInput").disabled = false;
        document.getElementById("clickMeButton").disabled = false;
    }
    
    
    
    // ++++++ HANDLE COMPUTER ++++++
    /* Wenn der Computer benutzt wird, wird er hier angezeigt und das Messageboard versteckt */
    function showComputerDisplay() {
        document.getElementById("messageBoard").style.display = "none";
        document.getElementById("computer").style.display = "block";
        document.getElementById("playground_shadow").style.backgroundColor = "black";
        document.getElementById("playground_shadow").classList.remove("shadow");
        
        disableInput();
    }
    
    /* Nutzer drückt auf eine Stelle der Binärzahl. Wenn es eine 0 war, wird es eine 1 und umgekehrt. */
    function setBinaryDigit(binaryId) {
        var binaryContainer = document.getElementById(binaryId);
        if(binaryContainer.innerHTML == "0"){
            binaryContainer.innerHTML = "1";
        } else {
            binaryContainer.innerHTML = "0";
        }
    }
    
    /* Nachdem eine Binärstelle gedrückt wird, wird dem Nutzer live die sich daraus errechnete Dezimalzahl angezeigt */
    function setCurrentResult(result) {
        var resultContainer = document.getElementById("computer_result");
        resultContainer.innerHTML = result;
    }
    
    /* Nachdem eine Zahl richtig "gehacked" wurde, werden alle Binärstellen auf 0 gesetzt */
    function resetBinaryDigits() {
        document.getElementById("computer_64").innerHTML = 0;
        document.getElementById("computer_32").innerHTML = 0;
        document.getElementById("computer_16").innerHTML = 0;
        document.getElementById("computer_8").innerHTML = 0;
        document.getElementById("computer_4").innerHTML = 0;
        document.getElementById("computer_2").innerHTML = 0;
        document.getElementById("computer_1").innerHTML = 0;
        document.getElementById("computer_result").innerHTML = 0;
    }
    
    /* Nachdem ein bestimmter Counter erreicht wurde (richtige Anzahl an Hacken) wird eine versteckte Nachricht angezeigt */
    function displayHiddenText(score) {
        if(score>SCORE_HIGH) {
            document.getElementById("computer_secret").style.display = "block";
            if(scorePoints == 0){
                that.notifyAll("hiddenComputer");
                scorePoints++;
            }
        }
    }
    
    
    
    
    /* Input: Score. Der Score wird dem Nutzer angezeigt. */
    function showNewUserScore(score) {
        document.getElementById("userScore").innerHTML = score.toString() + " Punkte";
    }
    
    /* CSS Shake. Wird als visuelles Mittel benutzt, um den Nutzer auf z.B. eine fehlerhafte Eingabe aufmerksam zu machen */
    function shake(element){
        var oldClass;
        oldClass = element.className;
        /* do the shake */
        element.className += " shake";
        /* then remove it after 0.3s */
        setTimeout(function(){ element.className = oldClass;}, 130);
    }
    
    
    that.showNewUserScore = showNewUserScore;
    that.displayHiddenText = displayHiddenText;
    that.writePaperAnswer = writePaperAnswer;
    that.resetBinaryDigits = resetBinaryDigits;
    that.setCurrentResult = setCurrentResult;
    that.setBinaryDigit = setBinaryDigit;
    that.showComputerDisplay = showComputerDisplay;
    that.hideExerciseDisplay = hideExerciseDisplay;
    that.writeStudyAnswer = writeStudyAnswer;
    that.editTask = editTask;
    that.showUpdatedGameState = showUpdatedGameState;
    that.displaySystemMessage = displaySystemMessage;
    that.saveUsername = saveUsername;
    that.clearConsoleAndStartGame = clearConsoleAndStartGame;
    return that;
};